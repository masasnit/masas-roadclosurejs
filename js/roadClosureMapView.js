var mapView;
var mapView_helper;

var rcmv_mapView_extent;
var rcmv_mapView_isPrinting = false;

$.ajaxSetup({ cache: false });

(function() {
    var beforePrint = function() {
        console.log('Functionality to run before printing.');
        rcmv_mapView_isPrinting = true;
        //mapView.zoomToExtent( rcmv_mapView_extent, false );
        //mapView.render("mapListView");
	    //mapView.zoomToMaxExtent();
    };
    var afterPrint = function() {
        console.log('Functionality to run after printing');

        //mapView.zoomToExtent( rcmv_mapView_extent, false );
        rcmv_mapView_isPrinting = false;
    };

    if (window.matchMedia) {
        var mediaQueryList = window.matchMedia('print');
        mediaQueryList.addListener(function(mql) {
            if (mql.matches) {
                beforePrint();
            } else {
                afterPrint();
            }
        });
    }

    window.onbeforeprint = beforePrint;
    window.onafterprint = afterPrint;
}());

var rcmv_colourCodeStyleMap = new OpenLayers.StyleMap(
	{fill: true,
		fillColor: '${colourCode}',
		fillOpacity: 0.5,
		strokeColor: '${colourCode}',
		strokeWidth: 4
	});

// STYLE for the preview outline:

var rcmv_outlineStyleMap = new OpenLayers.StyleMap(
	{fill: false,
		strokeColor: '#000000',
		strokeWidth: 1
	});

var rcmv_renderer = (rcmv_renderer) ? [rcmv_renderer] : OpenLayers.Layer.Vector.prototype.renderers;

// we'll use this layer to draw the preview on.
var rcmv_previewLayer = new OpenLayers.Layer.Vector("Preview Layer", {
    	visibility: true,
        styleMap: rcmv_colourCodeStyleMap
        },
        renderer); // Show existing road-closures....

// we'll use this layer to draw outlines on the previews for better visibility
var rcmv_outlineLayer = new OpenLayers.Layer.Vector("Outline Layer", {
    styleMap: rcmv_outlineStyleMap
});

// Point Layer....
var rcmv_markerLayer = new OpenLayers.Layer.Markers("Markers"); // for icons


function rcMapView_init() {
    // Setup the data table...
    var oTable = $('#rcMapView_table').dataTable( {
        "bAutoWidth": false,
        "aoColumns": [
            {
                "sTitle": "Actions",   "mData": "link"
            },
            {
                "sTitle": "hiddenID",  "mData": "identifier", "bVisible": false
            },
            {
                "sTitle": "Title",     "mData": "title", sWidth: "150px",
                "mRender": function ( data, type, full ) {
                    return '<label>Title:</label>' + data;
                }
            },
            {
                "sTitle": "Details",   "mData": "details",
                "mRender": function ( data, type, full ) {
                    return '<label>Details:</label>' + data;
                }
            },
            {
                "sTitle": "Delay",     "mData": "delay", sWidth: "132px",
                "mRender": function ( data, type, full ) {
                    return '<label>Delay:</label>' + data;
                }
            },
            {
                "sTitle": "Expires",   "mData": "expires", sWidth: "132px",
                "mRender": function ( data, type, full ) {
                    return '<label>Expires:</label>' + data;
                }
            },
            {
                "sTitle": "Effective", "mData": "effective", sWidth: "132px",
                "mRender": function ( data, type, full ) {
                    return '<label>Effective:</label>' + data;
                }
            },
            {
                "sTitle": "Audience",  "mData": "audience",
                "mRender": function ( data, type, full ) {
                    return '<label>Audience:</label>' + data;
                }
            },
            { "sTitle": "payload",   "mData": "entryPayload", "bVisible": false }
        ],
        "aaSorting": [[5,'desc']],
        "bDestroy": true,
        "bInfo": false,
        "bPaginate": false,
        "fnRowCallback": rcMapView_tableRowCallback
    });

    $("#rcMapView_table_filter").addClass( 'hidden-print' );

    mapView_helper = new MASAS.MapHelperII();
    rcMapView_loadMap();

    mapView.updateSize();
}

function rcMapView_updateSize() {
    mapView.updateSize();
}

function rcMapView_loadMap() {

	mapView = new OpenLayers.Map( 'mapListView' );
	mapView.events.register( "move", mapView, function() {
	    if( !rcmv_mapView_isPrinting ) {
	        rcmv_mapView_extent = mapView.getExtent();
	    }

	    rcMapView_updateExtent( rcmv_mapView_extent );
	});

	mapView.events.register( "updatesize", mapView, function() {
	    rcmv_mapView_extent = mapView.getExtent();
//	    console.log( "UpdateSize() Called.");
	    if( rcmv_mapView_isPrinting ) {
	        mapView.zoomToExtent( rcmv_mapView_extent, false );
	        //mapView.zoomToMaxExtent();
	    }

	    rcMapView_updateExtent( rcmv_mapView_extent );
	});

	baseLayer = new OpenLayers.Layer.OSM( 'osm', 'https://a.tile.openstreetmap.org/${z}/${x}/${y}.png' );
	mapView.addLayer(baseLayer);

	mapView.addLayers( [rcmv_previewLayer, rcmv_outlineLayer, rcmv_markerLayer]);

	var lonlat = new OpenLayers.LonLat( app_Settings.defaultExtent.lon, app_Settings.defaultExtent.lat);
	lonlat.transform( new OpenLayers.Projection("EPSG:4326"), mapView.getProjectionObject() );
	mapView.setCenter( lonlat, app_Settings.defaultExtent.zoom );

}

function rcMapView_updateExtent( extent )
{
    extent = extent.transform( mapView.getProjectionObject(), new OpenLayers.Projection("EPSG:4326") );
    var extentStr = extent.toBBOX( 2, true );
    extentStr = extentStr.replace( /,/g, ' ' );
    $("#rcMapView_mapExtent").text( extentStr );
}

function rcMapView_listMyEntries()
{
    // Populate the time this page was created...
    $("#rcPrintNow").text( moment( new Date() ).format("YYYY-MM-DD HH:mm") );

    var masasNameValues = app_Settings.userData.name.split( ':' );

    $(".rcMasasUserTitle").text( masasNameValues[0] + ' - ' + masasNameValues[1] );

    $(".rcMasasUser").each( function( index, element ) {
        $(this).text( app_Settings.userData.name );
    });

    mapView_helper.ClearMap();

    hub = new MASAS.Hub();
    if (app_Settings.token) { // no point retrieving if we don't have a code.
        if (app_Settings.userData)
        {
            // NOTE: Author filter doesn't work right now with "update_allow".
            //       Other authors will be filter out from the responses...
            //var authorURI = app_Settings.userData.uri;

            var authorId = app_Settings.userData.id;

            var start = moment.utc().subtract( { days: app_Settings.dtStartDeltaDays, seconds: app_Settings.dtStartDeltaSeconds } );
            var end = moment.utc().add( { days: app_Settings.dtEndDeltaDays } );
            start.milliseconds( 0 );
            end.milliseconds( 0 );

            var currentDate = new Date();
            currentDate.setMilliseconds( 0 );

            var options = {
                'update_allow': authorId,
                'dtStart': start.toISOString().replace( ".000", "" ),
                'dtEnd': end.toISOString().replace( ".000", "" )
            };

            var entries = hub.GetEntries(options, rcMapView_getEntriesSuccess); //TODO: add callbacks for success/fail
        }
    }
    else {
        console.log("ListMyEntries() - no token");
    }
}

function rcMapView_getEntriesSuccess( xmlFeed, entries ) {

    var tableArray= [];

    for( var i = 0; i<entries.length; i++)
 	{
 	    var ent = entries[i];

        // IMPORTANT: We need to filter out any Author that isn't us..
        //            This may change in the future...
 	    if( ent.author.uri === app_Settings.userData.uri )
        {
            var tableRow = {detail: "", title: "", delay: "", expires: null, effective: null, identifier: "", entryPayload: null, link: ""};

            tableRow.identifier = ent.identifier;
            //tableRow.title = ent.title.en;
            tableRow.title = ent.GetTitle();
            //tableRow.details = '<div class="rc-table-col-desc">' + ent.content.en + '</div>';
            tableRow.details = '<div class="rc-table-col-desc">' + ent.GetContent() + '</div>';
            tableRow.delay = GenerateDelayField( ent );

            var expiryMoment = moment( ent.expires );
            tableRow.expires = expiryMoment.format("YYYY-MM-DD HH:mm");

            if( ent.effective ) {
                tableRow.effective = moment( ent.effective ).format( "YYYY-MM-DD HH:mm" );
            }
            else {
                // Show the "Created" date...
                tableRow.effective = moment( ent.published ).format( "YYYY-MM-DD HH:mm" );
            }

            tableRow.audience = "MASAS";
            if( ent.dissLevel == 'private' ) {
                tableRow.audience = "Private";
            }
            if( ent.dissLevel === "public" ) {
                tableRow.audience = "Public and MASAS";
            }

            tableRow.entryPayload = ent; // use later when parsing ...

            tableRow.link = "<button class=\"btn-card-action btn btn-primary btn-xs \" onclick=\"updateRoadClosure('" + ent.identifier + "');\">Update</button>" +
                            "<button class=\"btn-card-action btn btn-primary btn-xs \" onclick=\"showPrintPreview('" + ent.identifier + "');\">Print</button>";

            if( expiryMoment.isAfter( moment() ) ) {
                tableRow.link = "<button class=\"btn-card-action btn btn-primary btn-xs \" onclick=\"cancelEntry('" + ent.identifier + "');\">Expire</button>" + tableRow.link;

                // Only draw on map if the entry is not expired...
                rcMapView_drawPreview( ent );
            }

            tableRow.link = "<div id='rcMapView_rowActions' class='btn-toolbar' role='toolbar'>" +
                            tableRow.link +
                            "</div>"

            tableArray.push(tableRow); // add the entry
        }
 	}

 	var rcTable = $('#rcMapView_table').DataTable();
	rcTable.fnClearTable();
    rcTable.fnAddData( tableArray );
    rcTable.fnDraw();

    $('#rcMapView_table th:first-child').addClass( 'hidden-print' );
    $('#rcMapView_table td:first-child').addClass( 'hidden-print' );

    // Extents...
    // Center on the geometry...
    if( entries.length > 0 )
    {
        var markerExtent = rcmv_markerLayer.getDataExtent();
        var outlineExtent = rcmv_outlineLayer.getDataExtent();
        var mergedExtent = new OpenLayers.Bounds();
        mergedExtent.extend( markerExtent );
        mergedExtent.extend( outlineExtent );

        mapView.zoomToExtent( mergedExtent );
        if( map.getZoom() < 15 ) {
            map.zoomTo( 15 );
        }
    }

}

function rcMapView_tableRowCallback( nRow, aData, iDisplayIndex, iDisplayIndexFull )
{
    var colorValue = 'bg-unknown';

    var expiryMoment = moment( aData.entryPayload.expires );
    if( expiryMoment.isAfter( moment() ) )
    {
        switch( aData.entryPayload.colourCode ) {
            case 'Red':
                colorValue = 'bg-danger'
                break;
            case 'Yellow':
                colorValue = 'bg-warning'
                break;
            case 'Green':
                colorValue = 'bg-success'
                break;
            default:
                colorValue = 'bg-unknown'
                break;
        }
    }
    else {
        // Expired...
        colorValue = 'rc-table-row-expired';
    }

    $('td', nRow).addClass( colorValue );
}

function rcMapView_drawPreview( entry ) {
    if( entry )
    {
    	if (entry.geometry[0]){
    		// Create the Icon - it's needed regardless
		    var icon = getIcon( entry );

		    switch (entry.geometry[0].type)
			{
		        case "point":
		        	mapView_helper.DrawPoint(entry.geometry[0].data, icon);
		        	break;
		        case "line":
		        	mapView_helper.DrawLine(entry.geometry[0].data, entry.colourCode, icon);
		        	break;
		        case "polygon":
		        	mapView_helper.DrawPolygon(entry.geometry[0].data, entry.colourCode, icon);
		        	break;
		        case "circle":
		        	alert ("NOT IMPLEMENTED");
		        	break;
		        case "box":
		        	alert("NOT IMPLEMENTED");
		        	break;

			}
    	}

    }
};

$("#rcMapView_btnRefresh").click( function( event ) {
    rcMapView_listMyEntries();
});

$("#rcMapView_btnPrint").click( function( event ) {
    var isPressed = $("#rcMapView_btnPrint").attr( "aria-pressed" ) == "true";
    if( !isPressed ) {
        $("#mapPanel").width( 650 );
        mapView.updateSize();
    }
    else {
        $("#mapPanel").width( "100%" );
        mapView.updateSize();
    }
});

$("#rcMapView_btnToggleList").click( function( event ) {
    if( $("#rcMapView_table").hasClass('cards') )
    {
        $("#rcMapView_table").removeClass('cards');
        $("#rcMapView_table thead").toggle();
    }
});

$("#rcMapView_btnToggleCard").click( function( event ) {
    if( !$("#rcMapView_table").hasClass('cards') )
    {
        $("#rcMapView_table").addClass('cards');
        $("#rcMapView_table thead").toggle();
    }
});


var MASAS = MASAS || {};

MASAS.MapHelperII = function()
{

// PUBLIC Methods

	this.ClearMap = function(){
		// clear out the drawing layer on the map.
		if (rcmv_markerLayer){
			rcmv_markerLayer.clearMarkers();
			rcmv_markerLayer.redraw();
		}
		if (rcmv_previewLayer){
			rcmv_previewLayer.destroyFeatures();
			rcmv_previewLayer.redraw();
		}
		if (rcmv_outlineLayer){
			rcmv_outlineLayer.destroyFeatures();
			rcmv_outlineLayer.redraw();
		}
	};

	// given a single point, draw the icon, with URL to image
	// pointString is the text received via MASAS entry
	// icon is an OpenLayers.Icon
	this.DrawPoint = function(pointString, icon)
	{
		//console.log('.DrawPoint received ' + pointString);
		var pts = pointString.split(" ");

		var proj = new OpenLayers.Projection("EPSG:4326");
		var point = new OpenLayers.LonLat(pts[1], pts[0]);

		//map.setCenter(point.transform(proj, map.getProjectionObject()));

		rcmv_markerLayer.addMarker( new OpenLayers.Marker(point.transform(proj, mapView.getProjectionObject()), icon) );

	};

	this.DrawLine = function(lineString, colour, icon)
	{
		//console.log(".DrawLine received linestring: " + lineString);
		var llPoints = this.ConvertGeometryStringToArrayLL(lineString);
		var line = new OpenLayers.Geometry.LineString(llPoints);
		var feature = new OpenLayers.Feature.Vector(line, {colourCode: colour});
		rcmv_previewLayer.addFeatures(feature);
		//Create Outline
		var outlinePoints = this.ConvertGeometryStringToArrayLL(lineString);
		var outline = new OpenLayers.Geometry.LineString(outlinePoints);
		var outlineFeature = new OpenLayers.Feature.Vector(outline);
		rcmv_outlineLayer.addFeatures(outlineFeature);

        rcmv_markerLayer.addMarker( new OpenLayers.Marker( new OpenLayers.LonLat(llPoints[0].x, llPoints[0].y), icon.clone() ) );
        rcmv_markerLayer.addMarker( new OpenLayers.Marker( new OpenLayers.LonLat(llPoints[llPoints.length-1].x, llPoints[llPoints.length-1].y), icon.clone() ) );
	};

	this.DrawPolygon = function(polygonString, colour, icon)
	{
		//console.log(".DrawPolygon received linestring: " + polygonString);
		// test if first and last point are the same - if so, ignore last point
		var llPoints = this.ConvertGeometryStringToArrayLL(polygonString);
		// draw using the colour indicated (RED/YELLOW/GREEN)
		llPoints.splice(llPoints.length,1); // remove last item as polygon (georss) duplicates starting point.

		//  polygonFeatures[i] = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring]);

		var ring = new OpenLayers.Geometry.LinearRing(llPoints);

		var polygon = new OpenLayers.Geometry.Polygon([ring]);
		var feature = new OpenLayers.Feature.Vector(polygon, {colourCode: colour});
		//feature.attributes = {colourCode: colour};
		//console.log("about to add Polygon feature - should be " + colour);
		rcmv_previewLayer.addFeatures(feature);

		//Create outline
		var outlinePoints = this.ConvertGeometryStringToArrayLL(polygonString);
		outlinePoints.splice(outlinePoints.length,1); // remove last item as polygon (georss) duplicates starting point.
		var outlineRing = new OpenLayers.Geometry.LinearRing(outlinePoints);
		var outlinePolygon = new OpenLayers.Geometry.Polygon([outlineRing]);
		var outlineFeature = new OpenLayers.Feature.Vector(outlinePolygon);
		rcmv_outlineLayer.addFeatures(outlineFeature);

        var centroidPoint = outlineRing.getCentroid();
        rcmv_markerLayer.addMarker( new OpenLayers.Marker( new OpenLayers.LonLat(centroidPoint.x, centroidPoint.y), icon.clone() ) );
	};

	this.ConvertGeometryStringToArrayLL = function(geometryString)
	{
		// takes in space-delimited georss values and returns array of LonLon points


		var values = $.trim(geometryString).split(" ");
		//var values = geometryString.trim().split(" ");

		var proj = new OpenLayers.Projection("EPSG:4326");

		// should have an even number of points 0=lat, 1=lat
		// console.log('in ConvertPointStringToPoints() received: ' + geometryString );
		// console.log('in ConvertPointStringToPoints() length:' + values.length.toString() );
		var arrayLL = [];


		for (i=0; i < ( values.length/2 ); i++){
			// moving in pairs 2*i (lon is second: 2i+1, lat is first: 2i )
			latInd = 2 * i; // Longitude value index
			lonInd = 2 * i + 1;		// Latitude value index
			// console.log(latInd.toString() + " " + lonInd.toString())
			// console.log('adding LonLat of LON: ' + values[lonInd].toString() + ", LAT:" + values[ latInd ].toString());
			var ll = new OpenLayers.LonLat(values[ lonInd ], values[ latInd ]);
			var llProjected = ll.transform(proj, mapView.getProjectionObject());
			llPt = new OpenLayers.Geometry.Point( llProjected.lon, llProjected.lat );
			// console.log('in ConvertGeometryStringtoArrayLL x:' + llPt.x.toString() + ' y:' + llPt.y.toString());
			arrayLL.push(llPt);
		}

		return arrayLL;


	};

};