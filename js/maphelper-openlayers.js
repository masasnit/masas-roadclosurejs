/**
 * MASAS library
 * Updated: Dec 17, 2012
 * Independent Joint Copyright (c) 2012 MASAS Contributors.  Published
 * under the Modified BSD license.  See license.txt for the full text of the license.
 */

var MASAS = MASAS || {};

MASAS.MapHelper = function()
{
	
// PUBLIC Methods

	this.ClearMap = function(){
		// clear out the drawing layer on the map.
		if (scratchLayer){
			scratchLayer.destroyFeatures();
			scratchLayer.redraw();
		}

		if (markerLayer){
			markerLayer.clearMarkers();
			markerLayer.redraw();
		}

		if (previewLayer){
			previewLayer.destroyFeatures();
			previewLayer.redraw();
		}

		if (outlineLayer){
			outlineLayer.destroyFeatures();
			outlineLayer.redraw();
		}
	};
	
	// given a single point, draw the icon, with URL to image
	// pointString is the text received via MASAS entry
	// icon is an OpenLayers.Icon
	this.DrawPoint = function(pointString, icon )
	{
		this.ClearMap();
		//console.log('.DrawPoint received ' + pointString);
		var pts = pointString.split(" ");
		
		var proj = new OpenLayers.Projection("EPSG:4326");
		var point = new OpenLayers.LonLat(pts[1], pts[0]);

		markerLayer.addMarker( new OpenLayers.Marker(point.transform(proj, map.getProjectionObject()), icon) );
		
	};

	this.DrawLine = function(lineString, colour, icon)
	{
		this.ClearMap();
		//console.log(".DrawLine received linestring: " + lineString);
		var llPoints = this.ConvertGeometryStringToArrayLL(lineString);
		var line = new OpenLayers.Geometry.LineString(llPoints);
		var feature = new OpenLayers.Feature.Vector(line, {colourCode: colour});
		previewLayer.addFeatures(feature);
		//Create Outline
		var outlinePoints = this.ConvertGeometryStringToArrayLL(lineString);
		var outline = new OpenLayers.Geometry.LineString(outlinePoints);
		var outlineFeature = new OpenLayers.Feature.Vector(outline);
		outlineLayer.addFeatures(outlineFeature);

        markerLayer.addMarker( new OpenLayers.Marker( new OpenLayers.LonLat(llPoints[0].x, llPoints[0].y), icon.clone() ) );
        markerLayer.addMarker( new OpenLayers.Marker( new OpenLayers.LonLat(llPoints[llPoints.length-1].x, llPoints[llPoints.length-1].y), icon.clone() ) );
	};
	
	this.DrawPolygon = function(polygonString, colour, icon)
	{
		this.ClearMap();
		//console.log(".DrawPolygon received linestring: " + polygonString);
		// test if first and last point are the same - if so, ignore last point
		var llPoints = this.ConvertGeometryStringToArrayLL(polygonString);
		// draw using the colour indicated (RED/YELLOW/GREEN)
		llPoints.splice(llPoints.length,1); // remove last item as polygon (georss) duplicates starting point.
		
		//  polygonFeatures[i] = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring]);
		
		var ring = new OpenLayers.Geometry.LinearRing(llPoints);
		
		var polygon = new OpenLayers.Geometry.Polygon([ring]);
		var feature = new OpenLayers.Feature.Vector(polygon, {colourCode: colour});
		//feature.attributes = {colourCode: colour};
		//console.log("about to add Polygon feature - should be " + colour);
		previewLayer.addFeatures(feature);
		
		//Create outline
		var outlinePoints = this.ConvertGeometryStringToArrayLL(polygonString);
		outlinePoints.splice(outlinePoints.length,1); // remove last item as polygon (georss) duplicates starting point.
		var outlineRing = new OpenLayers.Geometry.LinearRing(outlinePoints);
		var outlinePolygon = new OpenLayers.Geometry.Polygon([outlineRing]);
		var outlineFeature = new OpenLayers.Feature.Vector(outlinePolygon);
		outlineLayer.addFeatures(outlineFeature);

        var centroidPoint = outlineRing.getCentroid();
        markerLayer.addMarker( new OpenLayers.Marker( new OpenLayers.LonLat(centroidPoint.x, centroidPoint.y), icon.clone() ) );
	};
	
	this.TestPointDraw = function(inputString){
		
		if (!inputString) {
			// test default
			inputString = "45.00619997912534 -76.99115753173828";
		}
		this.DrawPoint(inputString);
		
		
		
	};
	this.TestPolygonDraw = function(inputString){
		
		if (!inputString) {
			// test default
			inputString = "46.62326013 -79.50488281 45.77158453 -79.94433594 45.55660244 -78.27441406 46.29024755 -78.93359375 46.62326013 -79.50488281";
		}
		// 46.62326013 -79.50488281 45.77158453 -79.94433594 45.55660244 -78.27441406 46.29024755 -78.93359375 46.62326013 -79.50488281
		var llPoints = this.ConvertGeometryStringToArrayLL(inputString);
		// draw using the colour indicated (RED/YELLOW/GREEN)
		llPoints.splice(llPoints.length,1); // remove last item as polygon (georss) duplicates starting point.
		
		//  polygonFeatures[i] = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring]);
		
		var ring = new OpenLayers.Geometry.LinearRing(llPoints);
		
		var polygon = new OpenLayers.Geometry.Polygon([ring]);
		
		
		previewLayer.addFeatures([polygon]);
		previewLayer.refresh();
		
		
	};
	
	this.ConvertGeometryStringToArrayLL = function(geometryString)
	{
		// takes in space-delimited georss values and returns array of LonLon points
		
		
		var values = $.trim(geometryString).split(" ");
		//var values = geometryString.trim().split(" ");
		
		var proj = new OpenLayers.Projection("EPSG:4326");
		
		// should have an even number of points 0=lat, 1=lat 
		// console.log('in ConvertPointStringToPoints() received: ' + geometryString );
		// console.log('in ConvertPointStringToPoints() length:' + values.length.toString() );
		var arrayLL = [];
		
		
		for (i=0; i < ( values.length/2 ); i++){
			// moving in pairs 2*i (lon is second: 2i+1, lat is first: 2i )
			latInd = 2 * i; // Longitude value index
			lonInd = 2 * i + 1;		// Latitude value index
			// console.log(latInd.toString() + " " + lonInd.toString())
			// console.log('adding LonLat of LON: ' + values[lonInd].toString() + ", LAT:" + values[ latInd ].toString());
			var ll = new OpenLayers.LonLat(values[ lonInd ], values[ latInd ]);
			var llProjected = ll.transform(proj, map.getProjectionObject());
			llPt = new OpenLayers.Geometry.Point( llProjected.lon, llProjected.lat );
			// console.log('in ConvertGeometryStringtoArrayLL x:' + llPt.x.toString() + ' y:' + llPt.y.toString());
			arrayLL.push(llPt); 
		}
		
		return arrayLL;

		
	};
	
};