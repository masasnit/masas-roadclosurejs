			
var accessCode = "";
var selectedHub = "";

var options = {};
var currentEntry = null;

helper = new MASAS.MapHelper();

hubInfo=loadXMLDoc("./config/hubConfig.xml"); //load hub information

// cookie vars
var cookieDefaultMapExtent = null;
var cookieAccessCode = null;
var cookieHub = null;

var colourKey = {
    "gray":   { title: "Unknown", desc: "Unknown road condition.", css: "road-map-heading-unknown" },
    "green":  { title: "Expect open", desc: "Responders can expect open.", css: "road-map-heading-success" },
    "yellow": { title: "Expect delays", desc: "Responders can expect delays.", css: "road-map-heading-warning" },
    "red":    { title: "Impassable", desc: "Responders can expect impassable.", css: "road-map-heading-danger" }
};

var rcCanOpsOpen511LinkTitle = "CanOps Open511 Event";

$.ajaxSetup({ cache: false });

$(document).ready(function(){
    init();
});


// Non-MAP Based functionality (or those abstracted away)

function getInitCookies()
{
	if ($.cookie('accessCode')) {
		cookieAccessCode = $.cookie('accessCode');
		//console.log('in getInitCookies() - we have an access code');
		
	} else {
		cookieAccessCode = null;
		////console.log('in getInitCookies() - NO access code');
	}
	
	if ($.cookie('selectedHub')) {
		cookieHub = $.cookie('selectedHub');
		//console.log('in getInitCookies() - we have a selected hub');
		
	} else {
		cookieHub = null;
		//console.log('in getInitCookies() - NO selected hub');
	}
}

//set the extent cookie
function setExtentCookie(name, value, expires){
	document.cookie = name + "=" + escape(value) + "; path=/" + ((expires == null) ? "" : "; expires=" + expires.toGMTString());
}

//return the extent cookie
function getExtentCookie(c_name){
	if (document.cookie.length>0){
		c_start=document.cookie.indexOf(c_name + "=");
		if (c_start!=-1){
			c_start=c_start + c_name.length+1;
			c_end=document.cookie.indexOf(";",c_start);
			if (c_end==-1) c_end=document.cookie.length;
				return unescape(document.cookie.substring(c_start,c_end));
			}
		}
	return "";
}

function checkExtentCookie(){
	if (getExtentCookie("SaveExtent") != ""){
		loadExtent();
	}
}

function changedDescription() {
	//console.log('Description changed to:' + $('#description').val());
	currentEntry.AreaDesc = $('#description').val();
	
};

function changedArea() {
	//console.log('AREA DESC changed to:' + $('#where-description').val());
	currentEntry.IncidentDesc = $('#description').val();
}

function changedExpires() { 
	//console.log('EXPIRES changed');
};

function init() {

    // Setup the date/time pickers...
    $('#dtExpires').datetimepicker({
        format: 'YYYY-MM-DD H:mm A'
    });

    $('#dtEffective').datetimepicker({
        format: 'YYYY-MM-DD H:mm A',
        showClear: true
    });

//    // Setup the data table...
//    var oTable = $('#tableClosures').dataTable( {
//        //"sDom": "<'row-fluid'<'span5'T><'span5'f>r>t<'row-fluid'<'span5'i><'span5'p>>",
//        "bAutoWidth": false,
//        "aoColumns": [
//            { "sTitle": "Actions",   "mData": "link" },
//            { "sTitle": "hiddenID",  "mData": "identifier", "bVisible": false },
//            { "sTitle": "Title",     "mData": "title", sWidth: "150px" },
//            { "sTitle": "Details",   "mData": "details" },
//            { "sTitle": "Delay",     "mData": "delay", sWidth: "132px" },
//            { "sTitle": "Expires",   "mData": "expires", sWidth: "132px" },
//            { "sTitle": "Effective", "mData": "effective", sWidth: "132px" },
//            { "sTitle": "Audience",  "mData": "audience" },
//            { "sTitle": "payload",   "mData": "entryPayload", "bVisible": false }
//        ],
//        "aaSorting": [[5,'desc']],
//        "bDestroy": true,
//        "bInfo": false,
//        "bPaginate": false,
//        "fnRowCallback": tableRowCallback
//    });
//
//    $("#tableClosures_filter").addClass( 'hidden-print' );

	getInitCookies();

	resetForm();

	CheckAccessCode(); 

	$('#alert').hide(); // hide the Alert Error section.

	// Assign handlers for onchange event for key UI inputs.
	$('#where-description').change(changedArea);
	$('#description').change(changedDescription);

	$('#appTab a').click(function (e) {
	  //console.log('click event');
	  e.preventDefault();
	  /* attempted to do ListMyEntries() on .show (or.shown) but aborted due 
	   * to insane effort at what should have worked: 
	   * http://getbootstrap.com/2.3.2/javascript.html#tabs */
//	  ListMyEntries();
	  $(this).tab('show');

	  // Resize the Report Map View...
	  rcMapView_updateSize();
	  rcMapView_listMyEntries();
	});	

	initMap();
	checkExtentCookie();
	
	$('a[data-toggle="tab"]:first').tab('show');

	// Force a map resize...
	map.updateSize();

	// Map View...
	rcMapView_init();
};

$("#btnClearEffective").click( function( event ) {
    // Don't let the click event reach the parent...
    event.stopImmediatePropagation();

    // Clear the picker...
    setDefaultEffective();
});

$( "#searchBox" ).keypress( function( event ) {
    if( event.which == 13 )
    {
        event.preventDefault();
        var address = $('#searchBox').val(); searchAddress(address);
    }
});
//
//$("#btnRefresh").click( function( event ) {
//    ListMyEntries();
//});

$("#btnNew").click( function( event ) {
    resetForm();
});

$("#sendButton").click( function( event ) {
    // Validate the data...
    var validData = true;
    var errorValues = [];

    // Validate the Title...
    var title = $('#where-description').val();
    if( title.length == 0 ) {
        validData = false;
        errorValues.push( 'Title' );
    }

    // Validate the Geometry...
    if( currentEntry.geometry.length == 0 ) {
        validData = false;
        errorValues.push( 'Geometry' );
    }

    if( validData )
    {
        clearValidationError();
        sendRoadClosure();
    }
    else {
        validationError( errorValues );
    }
});

$("#rcClosureType").change( function () {
    //toggleColour( this.value )
    var selected = $(this).val();

    rcSymbol = app_Settings.roadSymbols[selected];
    emSymbol = app_Settings.emSymbols[ rcSymbol.emSymbolKey ];

    $("#imgObsType").attr( 'src', emSymbol.url );

    currentEntry.icon = rcSymbol.emSymbolKey;

    drawPreview( currentEntry );
})

/// resets the form contents and clears out newEntry
function resetForm() {
	newEntry = null;

	$("#sendButton").text( "Send" );
	$("#btnNew").addClass( "hidden" );

	$('#where-description').val("");
  	$('#description').val("");

  	$("#rcDissLevel").val( "internal" );

  	// TODO: toggle colour button back (stays on what was clicked...), reset expires, clear map, clear search address pane
  	currentEntry = new MASAS.Entry();//
	toggleColour( 'gray' );

	setDefaultExpiry();
	setDefaultEffective();

    currentEntry.icon = app_Settings.roadSymbols['default'].emSymbolKey;

	// MAP
	helper.ClearMap();
}

function applyPublicAudience()
{
	// Check to see if the "Public and MASAS" audience is needed...
	if( app_Settings.url.indexOf( app_Settings.open511PublishHub ) < 0 )
	{
        // Hide it otherwise...
        $("#rcDissLevel option[value='public']").hide();
	}
	else
	{
	    // Make sure it's visible...
        $("#rcDissLevel option[value='public']").show();
	}
}
			
function setDefaultEffective( incomingDateTime ) {
    // Clear the Effective date picker...
    var picker = $('#dtEffective').data('DateTimePicker');
    picker.date(null);
}

function setDefaultExpiry( incomingDateTime ) {
    var expiresDateTime = incomingDateTime;

	// use moment.js to get date format in desired form.
	if( !incomingDateTime )
	{
		var expiresMoment = moment().add( "minutes", app_Settings.defaultExpiryOffsetMinutes ); // default to x hours (2)
		expiresDateTime = expiresMoment.toDate();
	}

	// get the minimum expiry time (if applicable):
	mmtMinimum = moment();
	mmtMinimum.add( 'minutes', app_Settings.minimumExpiryMinutes );
	pickerStart = mmtMinimum.toDate();

    var picker = $('#dtExpires').data('DateTimePicker');
    picker.date( expiresDateTime );
    picker.minDate( pickerStart );
};

function CheckAccessCode(){
	var code = "";
	if ($.cookie('accessCode')) {
		// we already have an access code stored
		
		code = $.cookie('accessCode');
		app_Settings.token = code;
		ValidateAccessCode(code);
	} else {
		// we don't have an Access Code stored in a cookie - therefore prompt user
		//console.log("showing modalSignin...");
		$("#modalSignin").modal("show");
		
	}
};

function checkHubSelect(hubList){
	var hub = "";
	if ($.cookie('selectedHub')) {
		// we already have a previous hub stored
		hub = $.cookie('selectedHub');
		hubSelect(hub);
	} else {
		//if user only has access to one hub and none is stored
		if (hubList.length == 1){
			hubSelect(hubList[0]);
		}
		//if the user has access to more than one hub and none is stored
		else{	
		//console.log("asking user to choose hub");
			$("#modalHubSelect").modal("show");
		}
	}
};
			
function ValidateAccessCode(code) {
	// 
	if (code) {
		accessCode = code;
		//console.log( 'IN CheckAccessCode - checking login... ');
		
		hub = new MASAS.Hub();
		
		// instantiate the MASAS Hub object
		hub.CheckUserCredentials( code, loginSuccess, loginFailure );
		
	}
};

function loginSuccess(code, userData){
	app_Settings.userData = userData;
	
	//console.log('IN loginSuccess for user: ' + userData.name);
	$("#modalSignin").modal("hide");
	$("#textDisplayUsername").text(userData.name);
	$("#textDisplayUsername").append( "  <span class=\"caret\"></span>");
	$.cookie('accessCode', code, { expires: 1 }); // set expires to 1 day.
	
	app_Settings.token = code; 
	$("#loginError").css('visibility','hidden');
	
	//console.log('Set Access Code to ' + app_Settings.token);

	var hubList = populateHubs(userData);
	checkHubSelect(hubList);
	// ListMyEntries();
};

function loginFailure(failureMsg){
	// to SHOW modal (need to use in the callback actually) $('#myModal').modal('show')
	//console.log('loginFailure msg:' + failureMsg);
	//this.css('visibility', 'visible');
	$("#loginError").css('visibility','visible');
	
	$("#modalSignin").modal("show");

};

function logout(){
	$.cookie('accessCode', "", { expires: -1 }); // delete previous cookie
	$.cookie('selectedHub', "", { expires: -1 }); // delete previous cookie
	resetForm();
	app_Settings.token = ""; 
	$("#textAccessCode").val("");
	$("#textDisplayHub").text("No Hub Selected");
	$("#textDisplayHub").append( "  <span class=\"caret\"></span>");
	document.getElementById("textDisplayHub").className = "btn btn-default dropdown-toggle";
	$("#hubChangeList").empty();
	$("#textDisplayUsername").text("Not logged in");
	$("#modalSignin").modal("show");
};

//Grant user access to selected hub
function hubSelect(hub){
	hubURL = hubInfo.getElementsByTagName("url");
	hubDisplayName = getElementsByAttributeValue("value", "en", hubInfo);
	hubColour = hubInfo.getElementsByTagName("color");
	for(var i=0;i<hubDisplayName.length;i++){
		if (hubDisplayName[i].childNodes[0].nodeValue == hub){
			app_Settings.url = hubURL[i].childNodes[0].nodeValue; //grab the hub URL from the XML
			applyPublicAudience();
			buttonColourSelect(hubColour[i].childNodes[0].nodeValue); //grab the colour from the XML
		}
	}
	//hide the hub select menu
	$("#modalHubSelect").modal("hide");
	$("#textDisplayHub").text(hub);
	$("#textDisplayHub").append( "  <span class=\"caret\"></span>");
	$.cookie('selectedHub', hub, { expires: 1 }); // set expires to 1 day.
};

//Populate option field and hubList button with available hubs and returns the list of accessible hubs
function populateHubs(userData){
    var hubList = [];
    hubURL = hubInfo.getElementsByTagName("url");
    hubDisplayName = getElementsByAttributeValue("value", "en", hubInfo);
    hubColour = hubInfo.getElementsByTagName("color");
    $("#hubList option").remove(); //clear previous options
    for (var i=0; i<hubURL.length;i++){
        $.each(userData.hubs, function(index, item) { // Iterates through hubs
            if (hubURL[i].childNodes[0].nodeValue == item.url && item.post == 'Y'){
                hubList.push(hubDisplayName[i].childNodes[0].nodeValue);
                $("#hubList").append($("<option></option>").text(hubDisplayName[i].childNodes[0].nodeValue));// Append an object to the inside of the select box
                var ul = document.getElementById("hubChangeList");
                var newLI = document.createElement("LI");
                ul.appendChild(newLI);
                newLI.innerHTML = "<a onclick=\"hubSelect('"+ hubDisplayName[i].childNodes[0].nodeValue +"');\">" + hubDisplayName[i].childNodes[0].nodeValue + "</a>";
            }
        })
    };
    return hubList;
};

//Change to button and text colour when selected
function buttonColourSelect(colourCode){
	document.getElementById("textDisplayHub").style.background = colourCode;
	document.getElementById("textDisplayHub").style.border = colourCode;
	//change text colour if required
	if (colourCode == "#00FF00" || colourCode == "#FFFF00" || colourCode == "#FFBF00"){
		document.getElementById("textDisplayHub").style.color = "#000000";
	}
	else if (colourCode == "#005555" || colourCode == "#FF0000"){
		document.getElementById("textDisplayHub").style.color = "#FFFFFF";
	}
};

$('#rc_rdColor :input').change( function () {
    toggleColour( this.value )
})
			
function toggleColour( colour )
{
    var incomingColour = colour.toLowerCase();

	var desc="gray";
	var cssColor = "";
	
    if( colourKey[incomingColour] )
    {
        desc = colourKey[incomingColour].desc;
        cssColor = colourKey[incomingColour].css;
    }

    $("#colour-description").html(desc);

    $("#rcMapPanelHeader").removeClass( "road-map-heading-unknown road-map-heading-success road-map-heading-warning road-map-heading-danger" );
    $("#rcMapPanelHeader").addClass( cssColor );

    if( currentEntry )
    {
        currentEntry.colourCode = incomingColour;
        currentEntry.colourDesc = desc;

        // MAP
        drawPreview( currentEntry );
    }
}

function buildRoadClosure( entry )
{
    var dissLevel = $( "#rcDissLevel" ).val();

  	// need to update the Entry to ensure that the most recent text (area and description), expiry, and colourCode are used.

    title = escapeXml( $('#where-description').val() );
    description = escapeXml( $('#description').val() );
    entry.SetTitle( title );
	entry.SetContent( description );

	entry.status = "Actual";
	if( dissLevel === 'private' ) {
	    entry.status = "Draft";
	}
	
	// EXPIRES TIME:
	var expiresPicker = $('#dtExpires').data('DateTimePicker');
  	entry.expires = expiresPicker.date();

  	// EFFECTIVE TIME:
  	var effectivePicker = $('#dtEffective').data('DateTimePicker');
  	var effectiveDate = effectivePicker.date();
  	if( effectiveDate != null )
  	{
  		entry.effective = effectiveDate; //Make it effective at a selected time if one is chosen
  	}

  	// Dissemination Level...
  	entry.dissLevel = dissLevel;

    // Permission...
    var permission = $( "#rcPermissions" ).val();
    switch( permission )
    {
        case 'org':
            entry.permissions = MASAS.PermissionEnum.org;
            break;
        case 'all':
            entry.permissions = MASAS.PermissionEnum.all;
            break;
        default:
            entry.permissions = MASAS.PermissionEnum.user;
            break;
    }

  	return entry;
};      

function enableEntryFormControls( enabled )
{
    if( enabled )
    {
        $("#rcNewItemFields").removeAttr( "disabled" );
    }
    else
    {
        $("#rcNewItemFields").attr( "disabled", "disabled" );
    }
};

function sendRoadClosure()
{
	enableEntryFormControls( false );

	// Build up the Entry...
	buildRoadClosure( currentEntry );

    // Check public and Hub...
    if( ( app_Settings.url.indexOf( app_Settings.open511PublishHub ) > 0 ) &&
        currentEntry.dissLevel == "public" )
    {
        sendRoadClosure_Open511();
    }
    else {
        // Not public, only send to MASAS...
        sendRoadClosure_MASAS();
    }
}

function buildOpen511Event( entry )
{
    // Fix the geometry...
    var geoType = "Point";
    var strCoords = entry.geometry[0].data.split( " " );
    var coords = [];

    for( var i=0; i<strCoords.length; i+= 2 )
    {
        coords.push( [ parseFloat( strCoords[i+1] ), parseFloat( strCoords[i] ) ] );
    }

    switch( entry.geometry[0].type )
    {
        case "point":
            geoType = "Point";
            coords = coords[0];
            break;
        case "line":
            geoType = "LineString";
            break;
        case "polygon":
            geoType = "Polygon"
            coords = [ coords ];
            break;
    }

    var geography = {
        "type": geoType,
        "coordinates": coords
    }

    // Severity...
    // Color.
    var severity = "UNKNOWN";
    switch( currentEntry.colourCode )
    {
        case "green":
            severity = "MINOR";
            break;
        case "yellow":
            severity = "MODERATE";
            break;
        case "red":
            severity = "MAJOR";
            break;
    }

    // Start Date...
    // Effective or NOW.
    var startDate = moment().format( "YYYY-MM-DD" );
    if( entry.effective )
    {
        startDate = moment(entry.effective).format( "YYYY-MM-DD" );
    }

    // End Date...
    // Expires.
    var endDate = moment(entry.expires).format( "YYYY-MM-DD" );

    var eventData = {
        "status": "ACTIVE",
        "jurisdiction_id": app_Settings.open511DefaultJurisdiction,
        "event_type": "ROAD_CONDITION",
        "headline": entry.GetTitle(),
        "severity": severity,
        "geography": geography,
        "description": entry.GetContent(),
        "schedule": {
            "recurring_schedules": [{
                "start_date": startDate,
                "end_date": endDate,
                "daily_start_time": null,
                "daily_end_time": null,
                "days": null
            }]
        },
        "detour": null,
        "roads": null,
    };

    return eventData;
}

function getEventId( entry )
{
    var foundLink = undefined;
    var eventId = undefined;
    var eventUrl = undefined;

    // Need to figure out if this was published already...
    var entryLinks = entry.links;
    if( entryLinks )
    {
        entryLinks.forEach( function( link ) {
            if( link.title.indexOf( rcCanOpsOpen511LinkTitle ) == 0 )
            {
                foundLink = link;
            }
        } );
    }

    if( foundLink )
    {
        eventUrl = foundLink.href;
        urlSplit = eventUrl.split('/');
        eventId = urlSplit[urlSplit.length-3] + "/" + urlSplit[urlSplit.length-2];
    }

    return eventId;
}

function sendRoadClosure_Open511()
{
    try {
        var event = buildOpen511Event( currentEntry );
        var eventId = getEventId( currentEntry )

        var server = new Open511.Server();

        // new entry
        if( !eventId )
        {
            // instantiate the MASAS Hub object
            server.CreateEvent( event, postEventSuccess, postEventFail );
        }
        else
        {
            delete event.jurisdiction_id;

            // Entry exists and needs to be updated instead.
            server.UpdateEvent( eventId, event, postEventSuccess, postEventFail);
        }
    }
    catch( e )
    {
        postFail( "An Exception has occured: " + e.message );
        console.log( e );
    }
}

function postEventSuccess( result )
{
    var open511Link = new MASAS.Link();

    //open511Link.rel    = 'alternate';
    //open511Link.type   = 'application/json';
    open511Link.rel    = 'related';
    open511Link.type   = 'application/json';
    open511Link.title  = rcCanOpsOpen511LinkTitle;
    open511Link.href   = app_Settings.open511ServerUrl + result.url;

    currentEntry.links.push( open511Link );

	sendRoadClosure_MASAS();
}

function postEventFail( errorMsg )
{
    console.log( errorMsg );

	addAlert( "Error!", "The entry was not added to the Public Feed.  An error occurred: " + errorMsg, "alert-danger" );

	// Send the MASAS Item anyways...
	sendRoadClosure_MASAS()
}

function sendRoadClosure_MASAS()
{
    try {
        //buildRoadClosure(currentEntry);// builds out the MASAS Entry text based on the data in the Road Closure (area, incident description, colourCode)

        hub = new MASAS.Hub();
        hub.userData = app_Settings.userData;

        // new entry
        if( !currentEntry.identifier )
        {
            // instantiate the MASAS Hub object
            hub.CreateEntry( currentEntry, postSuccess, postFail );
        }
        else
        {
            // Entry exists and needs to be updated instead.
            hub.UpdateEntry( currentEntry, postSuccess, postFail);
        }
    }
    catch( e )
    {
        postFail( "An Exception has occured: " + e.message );
        console.log( e );
    }
}

function postSuccess( result, entry ) {
	resetForm();

	enableEntryFormControls( true );
	notifySendEntrySuccess( entry );
};

function postFail( errorMsg )
{
    console.log( errorMsg );

	enableEntryFormControls( true );

	addAlert( "Error!", "An error occurred: " + errorMsg, "alert-danger" );
};

//function ListMyEntries()
//{
//    hub = new MASAS.Hub();
//    if (app_Settings.token) { // no point retrieving if we don't have a code.
//        if (app_Settings.userData)
//        {
//            // NOTE: Author filter doesn't work right now with "update_allow".
//            //       Other authors will be filter out from the responses...
//            //var authorURI = app_Settings.userData.uri;
//
//            var authorId = app_Settings.userData.id;
//
//            var start = moment.utc().subtract( { seconds: app_Settings.dtStartDeltaSeconds } );
//            var end = moment.utc().add( { days: app_Settings.dtEndDeltaDays } );
//            start.milliseconds( 0 );
//            end.milliseconds( 0 );
//
//            var currentDate = new Date();
//            currentDate.setMilliseconds( 0 );
//
//            var options = {
//                'update_allow': authorId,
//                //'dtinstance': currentDate.toISOString().replace( ".000", "" )
//                'dtStart': start.toISOString().replace( ".000", "" ),
//                'dtEnd': end.toISOString().replace( ".000", "" )
//            };
//
//            var entries = hub.GetEntries(options, getEntriesSuccess); //TODO: add callbacks for success/fail
//        }
//    }
//    else {
//        console.log("ListMyEntries() - no token");
//    }
//}
	
function editEntry(entryID)
{
    if( app_Settings.userURI ) {
        var options = {'author': app_Settings.userURI};
    }

    hub = new MASAS.Hub();

    currentEntryID = entryID;

    var entries = hub.GetEntries(options, editEntryByIdentifierSuccess); //TODO: add callbacks for success/fail
};
			
function editEntryByIdentifierSuccess(xmlFeed, entries ) {
	//TODO: refactor (cancel|getEntry duplicates...)
	
	var foundEntry = undefined;
	
	for( var i = 0; i<entries.length; i++){
		var ent = entries[i];
		if (ent.identifier === currentEntryID) {
			foundEntry = ent;
			
		}
	}
	
	if( foundEntry )
	{
		currentEntry = ent;
	}
	else {
		currentEntry = undefined;
	}
	
	if (currentEntry){
		loadEntryIntoForm(currentEntry);
	}
		 		
};
	
function cancelEntry( entryID )
{
    hub = new MASAS.Hub();

	hub.GetEntry( entryID, function( entry ) {

	        entry.expires = new Date();
            if(entry.effective != undefined){
                entry.effective = new Date(new Date().setDate(new Date().getDate()-5));
            }

            var content = "Road Closure Canceled in Road Closure Tool.\n\n" + entry.content.en;
            entry.content.en = content;

            hub.UpdateEntry( entry, function(){
                    // Success...
                    rcMapView_listMyEntries(); // refresh the list of entries.
                },
                function( message ) {
                    // Failure...
                    alert( "The item could not be cancelled. \n\nReason:" + message );
                }
            );
	    },
	    function( message ) {
	        // Failure...
	        alert( "The item could not be cancelled. \n\nReason:" + message );
	    }
	);
};

function getEntry(entryID) {
	hub = new MASAS.Hub();

	hub.GetEntry( entryID,
	    function( entry ) {
	        currentEntryID = entryID;
            currentEntry = entry;
            loadEntryIntoForm( entry );
	    },
	    function( message ) {
	        // Failure...
	        alert( "The item could not be retreived. \n\nReason:" + message );
	    }
	);
	
};
			
function getEntryByIdentifierSuccess(xmlFeed, entries )
{
	var currentEntry = undefined;

	for( var i = 0; i<entries.length; i++)
	{
		var ent = entries[i];
		if (ent.identifier === currentEntryID) {
			currentEntry = ent;
		}
	}
}

function loadEntryIntoForm(entry)
{
	resetForm();

	$("#sendButton").text( "Update" );
	$("#btnNew").removeClass( "hidden" );

	// get the key informal structure values out
	currentEntry = entry;
	
	// put the values into the form.
	$('#where-description').val( entry.GetTitle() );
	$('#description').val( entry.GetContent() );

	toggleColour(entry.colourCode);

    // Expires...
    var expiresPicker = $('#dtExpires').data('DateTimePicker');
	expiresPicker.date( entry.expires );

    // Effective...
    if( entry.effective )
    {
        var effectivePicker = $('#dtEffective').data('DateTimePicker');
	    effectivePicker.date( entry.effective );
    }

    // Audience...
    var dissLevel = entry.dissLevel;
    if( entry.status == 'draft' ) {
        dissLevel = 'private';
    }
	$("#rcDissLevel").val( dissLevel );

    // Permissions...
    var permissions = 'user';
    switch( entry.permissions ) {
        case MASAS.PermissionEnum.org:
            permissions = 'org'
            break;
        case MASAS.PermissionEnum.all:
            permissions = 'all'
            break;
    }
    $("#rcPermissions").val( permissions );

	//MAP
	drawPreview(entry);

    // Center on the geometry...
    if( entry.geometry[0].type === 'point' ) {
        map.zoomToExtent( markerLayer.getDataExtent() );
        map.zoomTo( 15 );
    }
    else {
        map.zoomToExtent( outlineLayer.getDataExtent() );
    }

    $('#appTab a[href="#editItem"]').tab('show'); // Select tab by name
}

function GenerateDelayField( entry )
{
    var delayStr = "";
    var colourCode = entry.colourCode;
    var key = colourCode.toLowerCase();

    var title = "";

    if( key in this.colourKey ) {
        title = this.colourKey[colourCode.toLowerCase()].title;
    }

    delayStr = '[' + colourCode.toUpperCase() + '] ' + title;

    return delayStr;
}

function updateRoadClosure(entryID)
{
    getEntry( entryID );
}

$('#rc_grpDrawTools :button').click( function () {
    getGeometry( this.value );
});

function onPrintPreviewClick( button )
{
    showPrintPreview( $(button).attr( 'masas-entry-id' ) );
}

function showPrintPreview( entryId )
{
    var url = 'roadClosurePreview.html?entryId=' + entryId;
	window.open( url, "_blank" );
}


function notifySendEntrySuccess( entry )
{
    var message = "Road Obstruction has been successfully sent to MASAS.";

    var newAlertEl = '<div class="alert alert-success fade in" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                        '<h4>Success!</h4>' +
                        '<p>' + message + '</p>' +
                        '<p><button id="rcPrintEntry" type="button" class="btn btn-default" masas-entry-id="' + entry.identifier + '" onclick="onPrintPreviewClick( this );" >Print</button></p>' +
                     '</div>';

    $("#rcAlertPanel").append( newAlertEl );
};

function addAlert( title, message, alertType )
{
    // alertType: alert-success, alert-info, alert-warning, alert-danger

    var newAlertEl = '<div class="alert ' + alertType + ' fade in" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                        '<h4>' + title + '</h4>' +
                        '<p>' + message + '</p>' +
                     '</div>';

    $("#rcAlertPanel").append( newAlertEl );
};

function validationError( values )
{
    // If it exists already, remove it...
    $("#rcAlertPanel > #rc_validationDiv").remove();

    var message = "<li>" + values.join( '</li><li>') + "</li>";

    var newAlertEl = '<div id="rc_validationDiv" class="alert alert-danger fade in" role="alert">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
                        '<h4>Validation Error!</h4>' +
                        '<p>One or more fields have missing or invalid values: <br><br><ul>' + message + '</ul></p>' +
                     '</div>';

    $("#rcAlertPanel").append( newAlertEl );
};

function clearValidationError()
{
    // If it exists already, remove it...
    $("#rcAlertPanel > #rc_validationDiv").remove();
};

function escapeXml(unsafe) {
    return unsafe.replace(/[<>&'"]/g, function (c) {
        switch (c) {
            case '<': return '&lt;';
            case '>': return '&gt;';
            case '&': return '&amp;';
            case '\'': return '&apos;';
            case '"': return '&quot;';
        }
    });
}