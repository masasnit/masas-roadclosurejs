function loadXMLDoc(dname)
{
    xhttp=new XMLHttpRequest();
    xhttp.open("GET",dname,false);
    xhttp.send();
    return xhttp.responseXML;
}

function getElementsByAttributeValue(attribute, value, xmlDocument)
{
  var allElements = xmlDocument.getElementsByTagName('*');
  var attributeElements = [];
  for (var i = 0; i < allElements.length; i++)
   {
    if (allElements[i].getAttribute(attribute) == value)
    {
      attributeElements.push(allElements[i]);
    }
  }
  return attributeElements;
}