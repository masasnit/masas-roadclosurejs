
var Open511 = Open511 || {};

///
/// Open511 Server object
///
Open511.Server = function()
{

    /// Public Members

    this.url = app_Settings.open511ServerUrl;
    this.token = undefined;

    /// Public Methods

    this.GetToken = function()
    {
        throw "Not Implemented";
    };

    this.VerifyToken = function()
    {
        throw "Not Implemented";

        var retValue = false;
        return retValue;
    };

    this.CreateEvent = function( event, callback_success, callback_fail )
    {
        var eventData = JSON.stringify( event );
        SendToServer( "POST", this.url + '/api/events/', 'application/json', eventData, callback_success, callback_fail);
    };

    this.UpdateEvent = function( id, event, callback_success, callback_fail )
    {
        var eventData = JSON.stringify( event );
        var updateUrl = this.url + '/api/events/' + id + "/";



        var fullURL = app_Settings.proxyURL + encodeURI( updateUrl );

        var request = $.ajax({
            type: "PATCH",
            url: fullURL,
            contentType: "application/json",
            headers: {
                'Content-Type': "application/json",
//                'X-HTTP-Method-Override': 'PATCH'
            },
            data: eventData,
            timeout: 120000
        });

        request.done( function(response, textStatus, jqXHR) {
            // This comes back as XML for now... let's get the url out if it..
            var values = $(response).find( "link[rel='self']").first();
            var href = $(values).attr( 'href' );

            if( callback_success && typeof( callback_success ) === "function" )
            {
                callback_success( { 'url': href } );
            }
        });

        request.fail( function(jqXHR, textStatus, errorThrown) {
            var failureMsg = 'Posting failed! ' + jqXHR.statusText + ': ' + jqXHR.responseText;

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });
    };

    this.GetEvents = function( options, callback_success, callback_fail )
    {
        throw "Not Implemented";
    };

    this.GetEvent = function( entryId, callback_success, callback_fail ) {
        throw "Not Implemented";
    };

    /// Private methods

    var SendToServer = function( type, url, contentType, payload, callback_success, callback_fail )
    {
        var fullURL = app_Settings.proxyURL + encodeURI( url );

        var request = $.ajax({
            type: type.toUpperCase(),
            url: fullURL,
            contentType: contentType,
            headers: { 'Content-Type': contentType },
            data: payload,
            timeout: 120000
        });

        request.done( function(response, textStatus, jqXHR) {
            if( callback_success && typeof( callback_success ) === "function" )
            {
                callback_success( response );
            }
        });

        request.fail( function(jqXHR, textStatus, errorThrown) {
            var failureMsg = 'Posting failed! ' + jqXHR.statusText + ': ' + jqXHR.responseText;

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });
        
    };
};
