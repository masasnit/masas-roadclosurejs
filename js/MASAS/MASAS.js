/**
 * MASAS library
 * Updated: Dec 17, 2012
 * Independent Joint Copyright (c) 2012 MASAS Contributors.  Published
 * under the Modified BSD license.  See license.txt for the full text of the license.
 */

var MASAS = MASAS || {};

///
/// MASAS Hub object
///
MASAS.Hub = function()
{

    /// Public Members

    this.userData = undefined;

    /// Public Methods

    this.CreateEntry = function( entry, callback_success, callback_fail )
    {
        var postData = '<?xml version="1.0" encoding="UTF-8"?>';
        postData += entry.ToXML( this.userData );

        if( "attachments" in entry && entry.attachments.length > 0 )
        {
            PostNewEntryWithAttachments( postData, entry.attachments, callback_success, callback_fail );
        }
        else
        {
            PostNewEntry( postData, 'application/atom+xml', callback_success, callback_fail );
        }
    };

    this.UpdateEntry = function( entry, callback_success, callback_fail )
    {
        var postData = '<?xml version="1.0" encoding="UTF-8"?>';
        postData += entry.ToXML( this.userData );

        var editUrl = entry.GetLink( "edit" );

        PutUpdateEntry( editUrl, postData, callback_success, callback_fail );
    };

    this.GetEntries = function( options, callback_success, callback_fail )
    {
        //console.log( 'GetEntries' );

        

				var destinationURL = app_Settings.url + "/feed";
				
        if( options != undefined )
        {
            var params = '?';
            if( options.hasOwnProperty( 'geoFilter' ) )
            {
                params +=  options.geoFilter;
            }
//options = { 'geoFilter': filter.type + '=' + filter.data.swLon + ',' + filter.data.swLat + ',' + filter.data.neLon + ',' + filter.data.neLat };
				if (options.hasOwnProperty('author'))
				{// only pull down the entries that are updatable. 
				// example: 
				// RAW: update_allow=https://access.masas-sics.ca/accounts/user/5/
				// ENCODED: update_allow=https%3A%2F%2Faccess.masas-sics.ca%2Faccounts%2Fuser%2F5%2F (encoded)
					var author = "author=" + options.author;
					
				}
						
	            if( params.length > 1 )
	            {
	            	//TODO: FIX this to handle >1 params (need & in between)
	                //url += params;
	            }
	        }
	//TODO: Refactor the param code above to be able to build up a param list - it is messy right now.
			if(app_Settings.token) {
				// need to add the secret
				destinationURL += "?secret=" + app_Settings.token;
				if (author){
					destinationURL += "&" + author;
				}		
			}

            if (options.hasOwnProperty('update_allow')){
				destinationURL += "&update_allow=" + options.update_allow;
			}

            // if a date range (dtStart to dtEnd) is supplied, use it.
			if (options.hasOwnProperty('dtinstance')){
				destinationURL += "&dtinstance=" + options.dtinstance;
			}

			if (options.hasOwnProperty('dtSince')){
				destinationURL += "&dtval=effective&dtsince=" + options.dtSince;
			}

			if (options.hasOwnProperty('dtStart'))
			{
				if (options.hasOwnProperty('dtEnd'))
				{
					destinationURL += "&dtval=effective&dtstart="+ options.dtStart + "&dtend=" + options.dtEnd;
				}
			}
			
			var url = app_Settings.proxyURL + encodeURIComponent(destinationURL); 
			//console.log("GET FROM:" + destinationURL);
			
				
    		var request = $.ajax({
            type: 'GET',
            url: url,
            headers: {
                'Authorization': 'MASAS-Secret ' + app_Settings.token
            },
            timeout: 120000
        });

        request.done( function( responseMsg ) {
            //console.log( 'MASAS Entries successfully retrieved!' );
            //console.log(responseMsg);

            var entries = GetEntriesFromFeed( responseMsg );

            if( callback_success && typeof( callback_success ) === "function" )
            {
                callback_success( responseMsg, entries );
            }
        });

        request.fail( function(jqXHR, textStatus) {
            //console.log( jqXHR );
            //console.log( 'Fail status: ' + textStatus );

            var failureMsg = 'Failed to retrieve MASAS Entries! ' + jqXHR.statusText + ': ' + jqXHR.responseText;
            //console.log( failureMsg );

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });
    };

    this.GetAttachment = function( url, callback_success, callback_fail )
    {
        //console.log( 'GetAttachment' );

        var request = $.ajax({
            type: 'GET',
            url: url,
            headers: {
                'Authorization': 'MASAS-Secret ' + app_Settings.token
            },
            timeout: 120000
        });

        request.done( function( responseMsg ) {
            //console.log( 'MASAS Entry attachment successfully retrieved!' );

            if( callback_success && typeof( callback_success ) === "function" )
            {
                callback_success( responseMsg );
            }
        });

        request.fail( function(jqXHR, textStatus) {
            //console.log( jqXHR );
            //console.log( 'Fail status: ' + textStatus );

            var failureMsg = 'Failed to retrieve MASAS Entry attachment! ' + jqXHR.statusText + ': ' + jqXHR.responseText;
            //console.log( failureMsg );

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail();
            }

        });
    };

    this.GetEntry = function( entryId, callback_success, callback_fail ) {

        var entryGuid = entryId;
        if( entryId.indexOf( "masas:entry" >= 0 ) )
        {
            var values = entryId.split( ":" );
            entryGuid = values.pop();
        }

        var hubUrl = app_Settings.url + "/feed/" + entryGuid;
        if(app_Settings.token) {
            // need to add the secret
            hubUrl += "?secret=" + app_Settings.token;
        }

        var url = app_Settings.proxyURL + hubUrl;

        var request = $.ajax({
            type: 'GET',
            url: url,
            headers: {
                'Authorization': 'MASAS-Secret ' + app_Settings.token
            },
            timeout: 120000
        });

        request.done( function( responseMsg ) {
            var entry = GetEntryFromResponse( responseMsg );
            if( entry )
            {
                if( callback_success && typeof( callback_success ) === "function" )
                {
                    callback_success( entry );
                }
            }
            else
            {
                var failureMsg = 'Failed to retrieve MASAS Entry!';
                console.log( responseMsg );

                if( callback_fail && typeof( callback_fail ) === "function" )
                {
                    callback_fail( failureMsg );
                }
            }
        });

        request.fail( function(jqXHR, textStatus) {
            var failureMsg = 'Failed to retrieve MASAS Entry! ' + jqXHR.statusText + ': ' + jqXHR.responseText;

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });
    };

	this.CheckUserCredentials = function(code, callback_success, callback_fail) {
			//console.log('IN CheckUserCredentials');
			
			//callback_fail('stubbed callback_fail');
			
			
			var masasObj = this;
        var accessControlURL = app_Settings.accessControlURL + '?secret=' + code + '&query_secret=' + code;
     		
     		var url = app_Settings.proxyURL + encodeURIComponent(accessControlURL); //?secret=bubbab'); 
     		app_Settings.userURI = app_Settings.accessControlURL;
     		
     
        var request = $.ajax({
            type: 'GET',
            dataType: 'json',
            url: url,
            headers: {
                'Authorization': 'MASAS-Secret ' + app_Settings.token
            },
            timeout: 120000
        });

        request.done( function( responseMsg ) {
            //console.log( 'MASAS User data successfully retrieved!' );
			app_Settings.userData = responseMsg;
			
            masasObj.userData = responseMsg;
            app_Settings.userURI = app_Settings.userData.uri;
            

            if( callback_success && typeof( callback_success ) === "function" )
            {
                callback_success( code, masasObj.userData );
            }
        });

        request.fail( function(jqXHR, textStatus) {
            //console.log( jqXHR );
            //console.log( 'Fail status: ' + textStatus );

            var failureMsg = 'Failed to retrieve MASAS USer data! ' + jqXHR.statusText + ': ' + jqXHR.responseText;
            //console.log( failureMsg );

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });
			
			
			//http://masas3.trl.sfu.ca/MASAS-NIT/MASAS-X-NG/PROXY.PHP?https://access.masas-sics.ca/api/check_access/?&secret=bubbab&query%5Fsecret=bubbab
			/*
			 * {"groups": ["https://access.masas-sics.ca/accounts/group/1"], 
			 * 	"hubs": [{"url": "https://sandbox2.masas-sics.ca/hub", "post": "Y"}, {"url": "https://sandbox1.masas-sics.ca/hub", "post": "Y"}],
			 *  "id": 5, 
			 *  "name": "MASAS NIT - Darrell O'Donnell",
			 *  "uri": "https://access.masas-sics.ca/accounts/user/5/"}
			 * 
			 */
			
    };
		
		
    this.RefreshUserData = function( callback_success, callback_fail )
    {
        //console.log( 'RefreshUserData' );

        var masasObj = this;
        var url = app_Settings.accessControlURL + '?query_secret=' + app_Settings.token;

        var request = $.ajax({
            type: 'GET',
            url: url,
            headers: {
                'Authorization': 'MASAS-Secret ' + app_Settings.token
            },
            timeout: 120000
        });

        request.done( function( responseMsg ) {
            //console.log( 'MASAS User data successfully retrieved!' );

            masasObj.userData = responseMsg;

            if( callback_success && typeof( callback_success ) === "function" )
            {
                callback_success( masasObj.userData );
            }
        });

        request.fail( function(jqXHR, textStatus) {
            //console.log( jqXHR );
            //console.log( 'Fail status: ' + textStatus );

            var failureMsg = 'Failed to retrieve MASAS USer data! ' + jqXHR.statusText + ': ' + jqXHR.responseText;
            //console.log( failureMsg );

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });
    };

    /// Private methods

    var GetEntriesFromFeed = function( xmlFeed )
    {
        var entries = [];

        $feedEntries = $(xmlFeed).find( "entry" );

        for( var entryCtr = 0; entryCtr < $feedEntries.length; entryCtr++ )
        {
            var newEntry = new MASAS.Entry();
            newEntry.FromNode( $feedEntries[entryCtr], app_Settings.userData );

            entries.push( newEntry );
            
            
            
        }

        return entries;
    };

    var GetEntryFromResponse = function( xmlResponse )
    {
        var entry = undefined;

        $feedEntry = $(xmlResponse).find( "entry" );

        if( $feedEntry.length > 0 )
        {
            entry = new MASAS.Entry();
            entry.FromNode( $feedEntry[0], app_Settings.userData );
        }

        return entry;
    };

    var PostNewEntry = function( entryData, contentType, callback_success, callback_fail )
    {
        //console.log( 'PostNewEntry' );
        //console.log( entryData);
        
        PostEntry( "post", app_Settings.url + '/feed', contentType, entryData, callback_success, callback_fail);
        
    };

    var PostNewEntryWithAttachments = function( entryData, attachments, callback_success, callback_fail )
    {
        var newData = '';
        newData  = '--0.a.unique.value.0\r\n';
        newData += 'Content-Disposition: attachment; name="entry"; filename="entry.xml"\r\n';
        newData += 'Content-Type: application/atom+xml\r\n';
        newData += '\r\n';
        newData += entryData;
        newData += '\r\n';

        for( var i=0; i<attachments.length; i++ )
        {
            newData += '--0.a.unique.value.0\r\n';
            newData += 'Content-Disposition: attachment; name="attachment"; filename="' + attachments[i].title + '"\r\n';
            newData += 'Content-Type: ' + attachments[i].contentType + '\r\n';
            //newData += 'Content-Description: ' + attachments[i].description + '\r\n';
            newData += 'Content-Transfer-Encoding: base64\r\n';
            newData += '\r\n';
            newData += attachments[i].base64;
            newData += '\r\n';
        }
        newData += '--0.a.unique.value.0--\r\n';

        PostNewEntry( newData, 'multipart/related; boundary=0.a.unique.value.0', callback_success, callback_fail );
    };

    var PutUpdateEntry = function( editUrl, postData, callback_success, callback_fail )
    {
        //console.log( 'PostUpdateEntry' );

        PostEntry( "PUT", editUrl, "application/atom+xml;type=entry", postData, callback_success, callback_fail);
    };

    var PostEntry = function( type, incomingurl, contentType, entryData, callback_success, callback_fail )
    {
        
				var fullURL = app_Settings.proxyURL + encodeURI(incomingurl + "?secret=" + app_Settings.token); 
				//console.log( 'PostEntry to ' + fullURL + " as " + type );
				
				/*
				// APPROACH BASED ON USAGE IN MASAS JS POSTING TOOL
				var do_post = new OpenLayers.Request.POST({
                url: fullURL,
                //proxy: MASAS.AJAX_PROXY_URL,
                headers: {'Content-Type': 'application/atom+xml'},
                data: entryData //,
                //callback: MASAS.post_entry_result
       });
				*/
				
		//console.log("in MASAS.js - PostEntry type:" + type.toUpperCase() + " URL:" + fullURL);
				
        var request = $.ajax({
            type: type.toUpperCase(),
            url: fullURL,
            contentType: contentType,
            headers: {'Content-Type': contentType,
                'Authorization': 'MASAS-Secret ' + app_Settings.token },
            data: entryData,
            timeout: 120000
        });

        request.done( function(response, textStatus, jqXHR) {
            //console.log(textStatus);
            //console.log('Posted to MASAS Hub successfully!');

            if( callback_success && typeof( callback_success ) === "function" )
            {
                var entry = GetEntryFromResponse( response );
                callback_success( response, entry );
            }
        });

        request.fail( function(jqXHR, textStatus, errorThrown) {
            //console.log( jqXHR );
            //console.log( 'Fail status: ' + textStatus );
            //console.log( 'ERROR THROWN: ' + errorThrown);

            var failureMsg = 'Posting failed! ' + jqXHR.statusText + ': ' + jqXHR.responseText;
            //console.log( failureMsg );

            if( callback_fail && typeof( callback_fail ) === "function" )
            {
                callback_fail( failureMsg );
            }

        });
        
    };
};
