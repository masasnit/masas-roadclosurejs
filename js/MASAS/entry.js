/**
 * MASAS - Entry Model object definition
 * Updated: Dec 18, 2012
 * Independent Joint Copyright (c) 2012 MASAS Contributors.  Published
 * under the Modified BSD license.  See license.txt for the full text of the license.
 */

var MASAS = MASAS || {};

MASAS.Author = function()
{
    this.name = "";
    this.uri = "";
};

MASAS.Attachment = function()
{
    this.uri            = '';
    this.title          = '';
    this.contentType    = '';
    this.length         = 0;
    this.base64         = undefined;
};

MASAS.Link = function()
{
    this.rel    = '';
    this.type   = '';
    this.title  = '';
    this.href   = '';
};

MASAS.PermissionEnum = Object.freeze( {
    "user": 0,
    "org": 1,
    "all": 2 }
);

MASAS.Entry = function()
{
    // Private members...
    var node = undefined;

    // Public members...
    this.identifier = undefined;
    this.editLink = undefined; // needed for Updates and Cancels.

    this.languages = [];

    this.title = {};    // Use language as the lookup key.
    this.content = {};
    this.summary = {};

    this.icon       = 'ems.other.other';
    this.status     = 'Test';
    this.severity   = undefined;
    this.certainty  = undefined;
    this.categories = [];
    this.colourCode = ''; // v2.2.1 hub
    this.dissLevel  = 'internal'; // Experimental Tag: [public, internal]

    this.published  = undefined;
    this.updated    = undefined;
    this.expires    = undefined;
    this.effective	= undefined;

    this.permissions = MASAS.PermissionEnum.user;

    this.attachments = [];
    this.links = [];

    this.geometry = [];

    this.author = new MASAS.Author();

    this.GetTitle = function( lang )
    {
        if( lang == undefined ) {
            lang = this.GetDefaultLanguage();
        }

        return this.title[lang];
    };

    this.SetTitle = function( title, lang )
    {
        if( lang == undefined ) {
            lang = this.GetDefaultLanguage();
        }

        this.title[lang] = title;
    };

    this.GetContent = function( lang )
    {
        if( lang == undefined ) {
            lang = this.GetDefaultLanguage();
        }

        return this.content[lang];
    };

    this.SetContent = function( content, lang )
    {
        if( lang == undefined ) {
            lang = this.GetDefaultLanguage();
        }

        this.content[lang] = content;
    };

    this.GetSummary = function( lang )
    {
        if( lang == undefined ) {
            lang = this.GetDefaultLanguage();
        }

        return this.summary[lang];
    };

    this.SetSummary = function( summary, lang )
    {
        if( lang == undefined ) {
            lang = this.GetDefaultLanguage();
        }

        this.summary[lang] = summary;
    };

    this.AddLanguage = function( lang )
    {
        var exists = false;
        for( var i = 0; i < this.languages.length; i++ )
        {
            if( this.languages[i] == lang ) {
                exists = true;
                break;
            }
        }

        if( !exists ) {
            this.languages.push( lang );
        }
    };

    this.GetDefaultLanguage = function()
    {
        var defaultValue = 'en';

        if( this.languages.length > 0 ) {
            defaultValue = this.languages[0];
        }
        else {
            // no languages, add "en" as default.
            this.languages.push( defaultValue );
        }

        return defaultValue;
    };

    this.GetLink = function( rel )
    {
        var retValue = "";

        if( node != undefined ) {
            retValue = $(node).find( "link[rel='"+ rel +"']" ).attr( "href" );
        }

        return retValue;
    };

    this.AddAttachment = function( attachment )
    {
        this.attachments.push( attachment );
    };

    this.RemoveAttachment = function( attachment )
    {
        var retValue = false;

        for( var i=0; i<this.attachments.length; i++ )
        {
            if( this.attachments[i].uri == attachment.uri )
            {
                this.attachments.splice( i, 1 );
                retValue = true;
                break;
            }
        };

        return retValue;
    };

	function serializeXml(xmldom){
		if (typeof XMLSerializer != "undefined"){
			return (new XMLSerializer()).serializeToString(xmldom);
	} else {
		return xmldom.xml;
		}
	}
	
    this.FromNode = function( entryNode, currentUser )
    {
        // reset the languages...
        this.languages = [];
        var entryXML = serializeXml(entryNode);
		////console.log("ENTRY XML: " + entryXML);
        // Keep a copy of the original node...
        node = $(entryNode).clone();

        // Let's populate the <span> with data...
        this.identifier = $(node).find( "id" ).text();
        this.editlink = $(node).find( "link[rel='edit']" ).attr( "href" ); // needed for update.
        //console.log('in Entry.FromNode() got Edit Link: ' + this.editlink);
        


        this.author.name = $(node).find( "author > name" ).text();
        this.author.uri = $(node).find( "author > uri" ).text();

        var titles = $(node).find( "title div[xml\\:lang]" );
        for( var titleCtr = 0; titleCtr < titles.length; titleCtr++ )
        {
            var lang = $( titles[titleCtr] ).attr( "xml:lang" );
            this.AddLanguage( lang );
            this.title[lang] = $( titles[titleCtr] ).text();
        }

        var contents = $(node).find( "content div[xml\\:lang]" );
        for( var contentCtr = 0; contentCtr < contents.length; contentCtr++ )
        {
            var lang = $( contents[contentCtr] ).attr( "xml:lang" );
            this.AddLanguage( lang );
            this.content[lang] = $( contents[contentCtr] ).text();
        }

        var summaries = $(node).find( "summary div[xml\\:lang]" );
        for( var summaryCtr = 0; summaryCtr < summaries.length; summaryCtr++ )
        {
            var lang = $( summaries[summaryCtr] ).attr( "xml:lang" );
            this.AddLanguage( lang );
            this.summary[lang] = $( summaries[summaryCtr] ).text();
        }

        // Single Categories...
        $statusNode = $(node).find( "category[scheme='masas:category:status']" );
        this.status = $statusNode.attr( "term" );

        // Dissemination Level
        this.dissLevel = 'internal';
		var statusContext = $statusNode.attr( "mea:context" );
		if( statusContext && statusContext.indexOf("masas:experimental:dl:") >= 0 )
		{
		    var values = statusContext.split( ":" );
		    this.dissLevel = values.pop();
		}

        this.icon = ConvertIcon_PubToInternal( $(node).find( "category[scheme='masas:category:icon']" ).attr( "term" ) );
	
		//colourCode
		//TODO: update when API expects colourCode vice colour
		var colourCodeNode = $(node).find( "category[scheme='masas:category:colour']" );
		if (colourCodeNode.length >0) {
			this.colourCode = $(colourCodeNode).attr("term");
		} else {
			// default to Gray for "no information supplied"
			this.colourCode = "Gray" ;
		}


        var certainty = $(node).find( "category[scheme='masas:category:certainty']" );
        if( certainty.length > 0 ) {
            this.certainty = $(certainty).attr( "term" );
        }
        else {
            this.certainty = undefined;
        }

        var severity = $(node).find( "category[scheme='masas:category:severity']" );
        if( severity.length > 0 ) {
            this.severity = $(severity).attr( "term" );
        }
        else {
            this.severity = undefined;
        }

        // Multiple Categories...
        var categories = $(node).find( "category[scheme='masas:category:category']" );
        for( var catCtr = 0; catCtr < categories.length; catCtr++ ) {
            this.categories.push( $(categories[catCtr] ).attr( "term" ) );
        }

        // Date/Time stamps...
        this.published  = new Date(moment($(node).find( "published" ).text()).format('YYYY/MM/DD HH:mm:ss'));
        this.updated    = new Date(moment($(node).find( "updated" ).text()).format('YYYY/MM/DD HH:mm:ss'));
        //console.log("<updated>.text() = " + $(node).find( "updated" ).text());
		//console.log("<age:expires>.text() = " + $(node).find("age\\:expires").text());
		this.expires = new Date(moment($(node).find( "age\\:expires, expires" ).text()).format('YYYY/MM/DD HH:mm:ss')); // have to escape and use namespace\:element
        var effective = $(node).find("met\\:effective, effective");
        if (effective.length > 0) {
        	
        	this.effective = new Date(moment(effective.text()).format('YYYY/MM/DD HH:mm:ss'));
        }
        else {
        	this.effective = null;
        }
		
        // Geometry...
        //TODO: This .find() test is NOT functioning on iOS devices...
        var geometry = $(node).find( "point, polygon, line, box, circle, georss\\:circle, georss\\:line, georss\\:point, georss\\:polygon" );
        for( var geoCtr = 0; geoCtr < geometry.length; geoCtr++ ) {
            this.geometry.push( { "type": geometry[0].localName, "data": $(geometry[0]).text() } );
        }

        // Attachments...
        var attachments = $(node).find( "link[rel='enclosure']" );

        for( var attachCtr = 0; attachCtr < attachments.length; attachCtr++ )
        {
            var attachment = new MASAS.Attachment();

            attachment.uri            = $(attachments[attachCtr]).attr( "href" );
            attachment.title          = $(attachments[attachCtr]).attr( "title" );
            attachment.contentType    = $(attachments[attachCtr]).attr( "type" );
            attachment.length         = $(attachments[attachCtr]).attr( "length" );

            this.attachments.push( attachment );
        }

        // Links...
        var links = $(node).find( "link[rel='related']" );

        for( var linksCtr = 0; linksCtr < links.length; linksCtr++ )
        {
            var link = new MASAS.Link();

            link.href   = $(links[linksCtr]).attr( "href" );
            link.title  = $(links[linksCtr]).attr( "title" );
            link.type   = $(links[linksCtr]).attr( "type" );
            link.rel    = $(links[linksCtr]).attr( "length" );

            this.links.push( link );
        }

        // Permissions...
        this.permissions = MASAS.PermissionEnum.user;

        var permissionStr = $(node).find('update, mec\\:update').text();

        if( permissionStr.indexOf( "all" ) > -1 ) {
            // All permission...
            this.permissions = MASAS.PermissionEnum.all;
        }
        else if( permissionStr.indexOf( currentUser.organizationID ) > -1 ) {
            // Group Permission...
            this.permissions = MASAS.PermissionEnum.org;
        }
        else if( permissionStr.indexOf( currentUser.id ) > -1 ) {
            // Individual user permission (not owner)...
            // TODO - THIS CASE IS CURRENTLY NOT SUPPORTED... Default to org...
            this.permissions = MASAS.PermissionEnum.org;
        }
    };

    this.ToXML = function( currentUser )
    {
        var entry = this;
        var entryXmlString = "";

		entryXmlString = GenerateXML( entry, currentUser );

	//TODO: reconcile the use of .GenerateXML in addition to UpdateNode - what is the utility? For now don't use it as it duplicates geometries.	
/*
 * This code is duplicating geometry
 * 
  
        if( node == undefined ) {
            entryXmlString = GenerateXML( entry );
        }
        else
        {
            var updatedNode = UpdateNode( entry );

            var xmlSerializer = new XMLSerializer();
            entryXmlString = xmlSerializer.serializeToString( updatedNode[0] );
        }
*/
        return entryXmlString;
    };

    /// Private methods

    var GenerateXML = function( entry, currentUser )
    {
        var xmlEntry = '';

        xmlEntry += '<entry ' +
                        'xmlns="http://www.w3.org/2005/Atom" ' +
                        'xmlns:mea="masas:experimental:attribute" ' +
                        'xmlns:met="masas:experimental:time" ' +
                        'xmlns:mec="masas:extension:control" ' +
                        'xmlns:app="http://www.w3.org/2007/app" ' +
                        '>';

        for( var iCat = 0; iCat < entry.categories.length; iCat++ ) {
            xmlEntry += '<category label="Category" scheme="masas:category:category" term="' + entry.categories[iCat] + '"/>';
        }

        var dissLevelStr = '';
        if( entry.dissLevel != undefined ) {
		    dissLevelStr += 'mea:context="masas:experimental:dl:' + entry.dissLevel + '"';
		}

        xmlEntry += '<category label="Status" scheme="masas:category:status" term="' + entry.status + '" ' + dissLevelStr + '/>';
        xmlEntry += '<category label="Icon" scheme="masas:category:icon" term="' + ConvertIcon_InternalToPub( entry.icon ) + '"/>';
        
		// TODO: Update when colour scheme is changed to colourCode
		xmlEntry += '<category label="ColourCode" scheme="masas:category:colour" term="' + $.trim(entry.colourCode.toLowerCase()) + '"/>';

        if( entry.certainty != undefined ) {
            xmlEntry += '<category label="Certainty" scheme="masas:category:certainty" term="' + entry.certainty + '"/>';
        }

        if( entry.severity != undefined ) {
            xmlEntry += '<category label="Severity" scheme="masas:category:severity" term="' + entry.severity + '"/>';
        }

        var iLangCtr = 0;
        xmlEntry += '<title type="xhtml">';
        xmlEntry += '<div xmlns="http://www.w3.org/1999/xhtml">';
        for( iLangCtr = 0; iLangCtr < entry.languages.length; iLangCtr++ ) {
            xmlEntry += '<div xml:lang="'+ entry.languages[iLangCtr] +'">' + entry.GetTitle( entry.languages[iLangCtr] ) + '</div>';
        }
        xmlEntry += '</div>';
        xmlEntry += '</title>';

        xmlEntry += '<content type="xhtml">';
        xmlEntry += '<div xmlns="http://www.w3.org/1999/xhtml">';
        for( iLangCtr = 0; iLangCtr < entry.languages.length; iLangCtr++ ) {
            xmlEntry += '<div xml:lang="'+ entry.languages[iLangCtr] +'">' + entry.GetContent( entry.languages[iLangCtr] ) + '</div>';
        }
        xmlEntry += '</div>';
        xmlEntry += '</content>';

        if( entry.GetSummary() && entry.GetSummary().length > 0 )
        {
            xmlEntry += '<summary type="xhtml">';
            xmlEntry += '<div xmlns="http://www.w3.org/1999/xhtml">';
            for( iLangCtr = 0; iLangCtr < entry.languages.length; iLangCtr++ ) {
                xmlEntry += '<div xml:lang="'+ entry.languages[iLangCtr] +'">' + entry.GetSummary( entry.languages[iLangCtr] ) + '</div>';
            }
            xmlEntry += '</div>';
            xmlEntry += '</summary>';
        }

        if( entry.expires != undefined ) {
        	var expires = moment(entry.expires)
            xmlEntry += '<expires xmlns="http://purl.org/atompub/age/1.0">' + expires.toJSON() + '</expires>';
        }
        if( entry.effective != undefined ) {
            xmlEntry += '<met:effective>' + entry.effective.toISOString() + '</met:effective>';
        }

        // Geometry...
        for( var iGeo = 0; iGeo < entry.geometry.length; iGeo++ ) {
            xmlEntry += '<' + entry.geometry[iGeo].type + ' xmlns="http://www.georss.org/georss">' + entry.geometry[iGeo].data + '</' + entry.geometry[iGeo].type + '>';
        }

        // Links...
        for( var iLink = 0; iLink < entry.links.length; iLink++ ) {
            var curLink = entry.links[iLink];
            xmlEntry += '<link ' +
                            'rel="' + curLink.rel + '" ' +
                            'type="' + curLink.type + '" ' +
                            'title="' + curLink.title + '" ' +
                            'href="' + curLink.href + '" ' +
                        ' />';
        }

        // Permissions...
        var permissionStr = "";

        switch( entry.permissions )
        {
            case MASAS.PermissionEnum.org:
                permissionStr = currentUser.organizationID;
                break;
            case MASAS.PermissionEnum.all:
                permissionStr = "all";
                break;
        }

        if( ( permissionStr.length > 0 ) || ( entry.status === 'Draft' ) )
        {
            xmlEntry += '<app:control>';

            if (permissionStr.length > 0) {
                xmlEntry += '<mec:update>' + permissionStr + '</mec:update>';
            }

            // Additional items as needed...
            if (entry.status === 'Draft') {
                xmlEntry += '<app:draft">yes</app:draft>';
            }

            xmlEntry += '</app:control>';
        }

        xmlEntry += '</entry>';

        return xmlEntry;
    };

    var UpdateNode = function( entry, currentUser )
    {
        // Generate a new node with the current info...
        var xml = GenerateXML( entry, currentUser );
        var xmlDoc = $.parseXML( xml );
        var newEntryNode = $( xmlDoc ).find( "entry" );

		
		var geomtypes = ['circle','polygon','line','point', 'georss:polygon', 'georss:circle', 'georss:point', 'georss:line']; // geometry types.


        // Create a target node using the existing node.
        var mergedNode = $(node).clone();

        // Clear out the entries that exists in the newEntryNode...
        $(mergedNode).children().each( function( index, element )
        {
            var curNode = $(this);
            
            // Find the current element in the "NEW" node...
            var foundNodes = $(newEntryNode).find( element.localName );
            if( $(foundNodes).length > 0 )
            {
                // If they exists in the "NEW" node, remove them from this one...
                $(curNode).each( function( index ) {
                    $(this).remove();
                });
            }
            
            // returned geometry may have a different geometry - need to strip any existing geometry out.
            /*if ( geomtypes.indexOf( element.localName ) != -1) {
            	// we have a geometry - delete this.
            	$(this).remove();
                
            } */


//            console.log('in entry.js UpdateNode() just compared' + element.localName + ' and found ' + $(foundNodes).length); 
            
        });

        // Merge the new node to the target node...
        $( newEntryNode ).children().appendTo( $( mergedNode ) );

        return mergedNode;
    };

    var ConvertIcon_InternalToPub = function( icon )
    {
        var returnVal = "";

        if( icon == "ems.other.other" || icon == "ems/other/other" )
        {
            returnVal = "other";
        }
        else
        {
            returnVal = icon.replace(/\./g, "/" );
        }

        return returnVal;
    };

    var ConvertIcon_PubToInternal = function( icon )
    {
        var returnVal = "";

        if( icon == undefined || icon == "other" )
        {
            returnVal = "ems.other.other";
        }
        else if( icon == "ems/incident/meteorological/snowfall" ) {
            returnVal = "ems.incident.meteorological.snowFall";
        }
        else if( icon == "ems/incident/meteorological/rainfall" ) {
            returnVal = "ems.incident.meteorological.rainFall";
        }
        else
        {
            returnVal = icon.replace(/\//g, "." );
        }

        return returnVal;
    };

};
