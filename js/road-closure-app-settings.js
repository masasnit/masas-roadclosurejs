
// declare the app_Settings
var app_Settings = {
	url: "",
	token: "",
	proxyURL: '/go?url=', // 
	open511ServerUrl: "https://open511.example.org",
	open511DefaultJurisdiction: "open511.example.org",
	open511PublishHub: "sandbox",
	accessControlURL: "https://access.masas-sics.ca/api/check_access/",
	defaultExpiryOffsetMinutes: 120,
	minimumExpiryMinutes: 30, //Expiry time must be greater than now + minimumExpiryMinutes.x
	dtStartDeltaSeconds: 30, //30 seconds
	dtEndDeltaDays: 60, //60 days
	defaultExtent: {
	    lat: 45.5683507, //45.4215,
	    lon: -76.7664988, //-75.6972,
	    zoom: 9
	},
	roadSymbols: {
		default: {
			emSymbolKey: 'ems.incident.roadway.roadwayClosure'
		},
		obstruction: {
			emSymbolKey: 'ems.incident.roadway.roadwayClosure'
		},
		condition: {
			emSymbolKey: 'ems.incident.roadway.hazardousRoadConditions'
		},
		detour: {
			emSymbolKey: 'ems.incident.roadway'
		},
		accident: {
			emSymbolKey: 'ems.incident.roadway.motorVehicleAccident'
		}
	},
	emSymbols: {
		'other': {
			url: './images/mapSymbols/ems.other.other.png',
			size: {	x: 32, y: 32 },
			offset: { dx: 16, dy: 16 }
		},
		'ems.other.other': {
			url: './images/mapSymbols/ems.other.other.png',
			size: {	x: 32, y: 32 },
			offset: { dx: 16, dy: 16 }
		},
		'ems.incident.roadway.roadwayClosure': {
			url: './images/mapSymbols/ems.incident.roadway.roadwayClosure.png',
			size: {	x: 32, y: 19 },
			offset: { dx: 16, dy: 9	}
		},
		'ems.incident.roadway.hazardousRoadConditions': {
			url: './images/mapSymbols/ems.incident.roadway.hazardousRoadConditions.png',
			size: {	x: 32, y: 28 },
			offset: { dx: 16, dy: 14 }
		},
		'ems.incident.roadway': {
			url: './images/mapSymbols/ems.incident.roadway.png',
			size: {	x: 32, y: 15 },
			offset: { dx: 16, dy: 7	}
		},
		'ems.incident.roadway.motorVehicleAccident': {
			url: './images/mapSymbols/ems.incident.roadway.motorVehicleAccident.png',
			size: {	x: 32, y: 17 },
			offset: { dx: 16, dy: 8	}
		}
	}
};


