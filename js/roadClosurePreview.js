			
var accessCode = "";
var selectedHub = "";

var options = {};
var currentEntry = null;

helper = new MASAS.MapHelper();

hubInfo=loadXMLDoc("./config/hubConfig.xml"); //load hub information

// cookie vars
var cookieDefaultMapExtent = null;
var cookieAccessCode = null;
var cookieHub = null;

var entryId = undefined;

var colourKey = {
    "gray":   { desc: "Unknown road condition.", css: "road-map-heading-unknown" },
    "green":  { desc: "Responders can expect open.", css: "road-map-heading-success" },
    "yellow": { desc: "Responders can expect delays.", css: "road-map-heading-warning" },
    "red":    { desc: "Responders can expect impassable.", css: "road-map-heading-danger" }
};

$.ajaxSetup({ cache: false });

$(document).ready(function(){
    init();
});

// Non-MAP Based functionality (or those abstracted away)

function getInitCookies()
{
	if ($.cookie('accessCode')) {
		cookieAccessCode = $.cookie('accessCode');
		//console.log('in getInitCookies() - we have an access code');
		
	} else {
		cookieAccessCode = null;
		////console.log('in getInitCookies() - NO access code');
	}
	
	if ($.cookie('selectedHub')) {
		cookieHub = $.cookie('selectedHub');
		//console.log('in getInitCookies() - we have a selected hub');
		
	} else {
		cookieHub = null;
		//console.log('in getInitCookies() - NO selected hub');
	}
}

//set the extent cookie
function setExtentCookie(name, value, expires){
	document.cookie = name + "=" + escape(value) + "; path=/" + ((expires == null) ? "" : "; expires=" + expires.toGMTString());
}

//return the extent cookie
function getExtentCookie(c_name){
	if (document.cookie.length>0){
		c_start=document.cookie.indexOf(c_name + "=");
		if (c_start!=-1){
			c_start=c_start + c_name.length+1;
			c_end=document.cookie.indexOf(";",c_start);
			if (c_end==-1) c_end=document.cookie.length;
				return unescape(document.cookie.substring(c_start,c_end));
			}
		}
	return "";
}

function checkExtentCookie(){
	if (getExtentCookie("SaveExtent") != ""){
		loadExtent();
	}
}

function init() {
    entryId = location.search.split('entryId=')[1]

	getInitCookies();

	resetForm();


	initMap();

	CheckAccessCode(); 

	checkExtentCookie();

	// Force a map resize...
	map.updateSize();

	clipboard = new Clipboard( "#btnCopyText" );

    clipboard.on('success', function(e) {
        // Show the map again...
        var $mapDiv = $("#map");
        $mapDiv.show();
    });
    clipboard.on('error', function(e) {
        // Show the map again...
        var $mapDiv = $("#map");
        $mapDiv.show();
    });

    // Populate the time this page was created...
    $("#rcPrintNow").text( moment( new Date() ).format("YYYY-MM-DD HH:mm") );
}

$("#btnPrint").click( function( event ) {
    window.print();
});

$("#btnCopyText").click( function( event ) {
    // Hide the map... we can't copy it..
    var $mapDiv = $("#map");
    $mapDiv.hide();
});

/// resets the form contents and clears out newEntry
function resetForm() {
	currentEntry = new MASAS.Entry();

	$('#where-description').text("");
  	$('#description').text("");

	setPanelColour( 'gray' );

    $('#dtEffective').text("");
    $('#dtExpires').text("");

	// MAP
	helper.ClearMap();
}

function CheckAccessCode(){
	var code = "";
	if ($.cookie('accessCode')) {
		// we already have an access code stored
		
		code = $.cookie('accessCode');
		app_Settings.token = code;
		ValidateAccessCode(code);
	} else {
		// we don't have an Access Code stored in a cookie - therefore prompt user
		//console.log("showing modalSignin...");
		$("#modalSignin").modal("show");
		
	}
}

function checkHubSelect(hubList){
	var hub = "";
	if ($.cookie('selectedHub')) {
		// we already have a previous hub stored
		hub = $.cookie('selectedHub');
		hubSelect(hub);
	} else {
		//if user only has access to one hub and none is stored
		if (hubList.length == 1){
			hubSelect(hubList[0]);
		}
		//if the user has access to more than one hub and none is stored
		else{	
		//console.log("asking user to choose hub");
			$("#modalHubSelect").modal("show");
		}
	}
}
			
function ValidateAccessCode(code) {
	// 
	if (code) {
		accessCode = code;
		//console.log( 'IN CheckAccessCode - checking login... ');
		
		hub = new MASAS.Hub();
		
		// instantiate the MASAS Hub object
		hub.CheckUserCredentials( code, loginSuccess, loginFailure );
		
	}
}

function loginSuccess(code, userData){
	app_Settings.userData = userData;

	$("#modalSignin").modal("hide");
	$("#textDisplayUsername").text(userData.name);
	$("#textDisplayUsername").append( "  <span class=\"caret\"></span>");

    $(".rcMasasUser").each( function( index, element ) {
        $(this).text( app_Settings.userData.name );
    });

	$.cookie('accessCode', code, { expires: 1 }); // set expires to 1 day.

	app_Settings.token = code;
	$("#loginError").css('visibility','hidden');

	//console.log('Set Access Code to ' + app_Settings.token);

	var hubList = populateHubs(userData);
	checkHubSelect(hubList);

	if( entryId ) {
	    getEntry( entryId );
	}
}

function loginFailure(failureMsg)
{
	$("#loginError").css('visibility','visible');
	$("#modalSignin").modal("show");
}

//Grant user access to selected hub
function hubSelect(hub){
	hubURL = hubInfo.getElementsByTagName("url");
	hubDisplayName = getElementsByAttributeValue("value", "en", hubInfo);
	hubColour = hubInfo.getElementsByTagName("color");
	for(var i=0;i<hubDisplayName.length;i++){
		if (hubDisplayName[i].childNodes[0].nodeValue == hub){
			app_Settings.url = hubURL[i].childNodes[0].nodeValue; //grab the hub URL from the XML
		}
	}
	//hide the hub select menu
	$("#modalHubSelect").modal("hide");
//	$("#textDisplayHub").text(hub);
//	$("#textDisplayHub").append( "  <span class=\"caret\"></span>");
	$.cookie('selectedHub', hub, { expires: 1 }); // set expires to 1 day.
}

//Populate option field and hubList button with available hubs and returns the list of accessible hubs
function populateHubs(userData){
    var hubList = [];
    hubURL = hubInfo.getElementsByTagName("url");
    hubDisplayName = getElementsByAttributeValue("value", "en", hubInfo);
    hubColour = hubInfo.getElementsByTagName("color");
    $("#hubList option").remove(); //clear previous options
    for (var i=0; i<hubURL.length;i++){
        $.each(userData.hubs, function(index, item) { // Iterates through hubs
            if (hubURL[i].childNodes[0].nodeValue == item.url && item.post == 'Y'){
                hubList.push(hubDisplayName[i].childNodes[0].nodeValue);
            }
        })
    };
    return hubList;
}

function setPanelColour( colour )
{
    var incomingColour = colour.toLowerCase();

	var desc = "gray";
	var cssColor = "";
	
    if( !(incomingColour in colourKey) ) {
        incomingColour = 'gray';
    }

    desc = '[' + incomingColour.toUpperCase() + '] ' + colourKey[incomingColour].desc;
    cssColor = colourKey[incomingColour].css;

    $("#colour-description").html(desc);

    $("#rcMapPanelHeader").removeClass( "road-map-heading-unknown road-map-heading-success road-map-heading-warning road-map-heading-danger" );
    $("#rcMapPanelHeader").addClass( cssColor );
}

function getEntry(entryID) {
	hub = new MASAS.Hub();

	hub.GetEntry( entryID,
	    function( entry ) {
	        currentEntryID = entryID;
            currentEntry = entry;
            loadEntryIntoForm( entry );
	    },
	    function( message ) {
	        // Failure...
	        alert( "The item could not be retreived. \n\nReason:" + message );
	    }
	);
	
}

function loadEntryIntoForm(entry)
{
	resetForm();

	// get the key informal structure values out 
	currentEntry = entry;
	
	// put the values into the form.
	$('#where-description').text( entry.GetTitle() );
	$('#description').text( entry.GetContent() );

	setPanelColour(entry.colourCode);

    // Expires...
    var expiryMoment = moment( entry.expires );
	var expires = expiryMoment.format("YYYY-MM-DD HH:mm");
    $('#dtExpires').text( expires );

    // Effective...
    if( entry.effective )
    {
        var effectiveMoment = moment( entry.effective );
	    var effective = effectiveMoment.format("YYYY-MM-DD HH:mm");
        $('#dtEffective').text( effective );
    }
    else
    {
	    var published = moment( entry.published ).format("YYYY-MM-DD HH:mm");
        $('#dtEffective').text( published );
    }

    // Dissemination level...
    $('#previewDissLevel').text( 'MASAS' );

    if( entry.dissLevel == 'private' ) {
        $('#previewDissLevel').text( 'Private' );
    }
    else if( entry.dissLevel == 'public' ) {
        $('#previewDissLevel').text( 'Public and MASAS' );
    }

	// Update the Map with the geometry...
	drawPreview(entry);

	// Center on the geometry...
	if( entry.geometry[0].type === 'point' ) {
         map.zoomToExtent( markerLayer.getDataExtent() );
         map.zoomTo( 15 );
	}
	else {
	    map.zoomToExtent( outlineLayer.getDataExtent() );
	}
}