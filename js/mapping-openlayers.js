// 
var map = null;
var baseLayer = null;
var geocoder = null;


// STYLE for preview:

var colourCodeStyleMap = new OpenLayers.StyleMap(
	{fill: true, 
		fillColor: '${colourCode}', 
		fillOpacity: 0.5, 
		strokeColor: '${colourCode}',
		strokeWidth: 4
	});

// STYLE for the preview outline:
	
var outlineStyleMap = new OpenLayers.StyleMap(
	{fill: false,   
		strokeColor: '#000000',
		strokeWidth: 1
	});	

var renderer = (renderer) ? [renderer] : OpenLayers.Layer.Vector.prototype.renderers;

var searchLayer = new OpenLayers.Layer.Markers("Search Layer"); //, {styleMap: colourCodeStyleMap});

var scratchLayer = new OpenLayers.Layer.Vector("Scratch Layer"); //, {styleMap: colourCodeStyleMap});

// we'll use this layer to draw the preview on.
var previewLayer = new OpenLayers.Layer.Vector("Preview Layer", {
    	visibility: true,
        styleMap: colourCodeStyleMap
        },
        renderer); // Show existing road-closures....
        
// we'll use this layer to draw outlines on the previews for better visibility
var outlineLayer = new OpenLayers.Layer.Vector("Outline Layer", {
        styleMap: outlineStyleMap
        });        

var markerLayer = new OpenLayers.Layer.Markers("Markers"); // for icons
var options = {};

// OpenLayer DrawControls.
var drawControls = {
	point: new OpenLayers.Control.DrawFeature(
		scratchLayer,OpenLayers.Handler.Point),
    line: new OpenLayers.Control.DrawFeature(scratchLayer,
    	OpenLayers.Handler.Path, options),
	polygon: new OpenLayers.Control.DrawFeature(scratchLayer,
		OpenLayers.Handler.Polygon, options)
};


function getGeometry( drawTool ) {
    helper.ClearMap();
        
    // de-activate all drawControls (activates again after)
    for( key in drawControls ) {
        var cont = drawControls[key];
        cont.deactivate();
    }

    if( drawTool in drawControls )
    {
        var control = drawControls[drawTool];
        control.activate();
    }
};

function initMap() {
	
	map = new OpenLayers.Map('map', {theme: null});

	/*
	baseLayer = new OpenLayers.Layer.Google('Google Map', {
		type: google.maps.MapTypeId.ROADMAP,
		isBaseLayer: true,
		//maxExtent: new OpenLayers.Bounds(-17772500.0,4975150.0,-4339200.0,11227000.0),//canada
					visibility: false
				});
	*/
	baseLayer = new OpenLayers.Layer.OSM( 'osm', 'https://a.tile.openstreetmap.org/${z}/${x}/${y}.png' );
	map.addLayer(baseLayer);
				  
	geocoder = new google.maps.Geocoder();
    
	map.addLayers([searchLayer, scratchLayer, previewLayer, outlineLayer, markerLayer]);
	  
	var lonlat = new OpenLayers.LonLat( app_Settings.defaultExtent.lon, app_Settings.defaultExtent.lat);
	lonlat.transform( new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject() );
	map.setCenter( lonlat, app_Settings.defaultExtent.zoom );


	// add the controls for drawing point, line, polygon
	scratchLayer.events.on(
		{"featureadded" : newFeatureAdded}
	);

	// attach handle to get geometry out.
	for(var key in drawControls) {
	  map.addControl(drawControls[key]);
	}

};

//Saves the extent in a cookie for the user to load later
function saveExtent(){
	var mapzoom=map.getZoom();
	var mapcenter=map.getCenter();
	var maplon=mapcenter.lon;
	var maplat=mapcenter.lat;
	var cookiestring=maplon+"_"+maplat+"_"+mapzoom;
	var exp = new Date(); //set new date object
	exp.setTime(exp.getTime() + (1000 * 60 * 60 * 24 * 365 * 2)); //set it 2 years ahead
	setExtentCookie("SaveExtent", cookiestring, exp);
}


//load a previously saved extent
function loadExtent(){
	var loadedstring=getExtentCookie("SaveExtent");
	var splitstr=loadedstring.split("_");
	var lonlat = new OpenLayers.LonLat(parseFloat(splitstr[0]), parseFloat(splitstr[1]));
	map.setCenter(lonlat, splitstr[2]);
}
 


//TODO: rejig for OL
function drawMapPreview(entry){
	// draw icon for road closure
	// Point - draw @ point
	// Line - draw @ mid-point
	// Polygon - draw @ centroid
	
	// draw colour-coded graphic for LINE or POLYGON
	// Point - do not draw (icon already done)
	 
			// Create the Icon - it's needed regardless
			alert('in drawMapPreview() line 81');
	//console.log("drawMapPreview() @line 81");
	if (entry)
	{
		if (entry.geometry[0]) {
			//console.log("in drawMapPreview - geometry type: " + entry.geometry[0].type + " COLOUR:" + entry.colourCode);
		
			switch (entry.geometry[0].type) 
	      	{
	      		case "point":
					helper.DrawPoint(entry.geometry[0].data, entry.colourCode);		
	      			break;
	      			
	      		case "line":
					helper.DrawLine(entry.geometry[0].data, entry.colourCode);
	      			break;
	      			
	      		case "polygon":
	      			helper.DrawPolygon(entry.geometry[0].data, entry.colourCode);
	      			break;
	      			
	      		case "circle":
					helper.DrawCircle(entry.geometry[0].data, entry.colourCode);		
	      			break;
	      	} //switch
	    }
      	else {
      		//console.log("in drawMapPreview() with entry, but no geometry.");
  		}
  	} 
				
};

/* address search - REPLACE with locale-specific search where available */
function searchAddress(addressString) {
	geocoder.geocode( { 'address': addressString}, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
			loc = results[0].geometry.location;
			
			//console.log("ok: " + loc);
			var lonlat = new OpenLayers.LonLat(loc.lng(), loc.lat());
			lonlat.transform( new OpenLayers.Projection("EPSG:4326"), map.getProjectionObject() );
			map.setCenter( lonlat, 15 );

			// Clear the previous, if any...
			clearSearchLayer();

			// Add a new marker...
			var size = new OpenLayers.Size(64,64);
			var offset = new OpenLayers.Pixel(-size.w/2, -size.h+((size.h/16)) );
		    var icon = new OpenLayers.Icon('./images/searchLocation.png', size, offset );

		    searchLayer.addMarker( new OpenLayers.Marker( lonlat, icon ) );
		}
		else
		{
			alert("Geocode was not successful for the following reason: " + status);
		}
    });
};

function clearSearchTool()
{
    $('#searchBox').val("");
    clearSearchLayer();
}

function clearSearchLayer()
{
    if( searchLayer )
    {
        searchLayer.clearMarkers();
        searchLayer.redraw();
    }
}

function newFeatureAdded(evt) //typeString, geometryString)
{
    // layer-level "featureadded" has an event.feature property (DrawControl has feature returned directly)
    ft = evt.feature;

    if (currentEntry){
        // need to clear the current geometry (if assigned)
        if (currentEntry.geometry.length > 0){
            currentEntry.geometry.pop(); // removes last element
            if (currentEntry.geometry.length != 0){
                //console.log('ERROR - in newFeatureAdded - we still have geometry???');
            }
        }
    }

    var geometryClass = ft.geometry.CLASS_NAME;
    var nodes = ft.geometry.getVertices(); // first (and only)

    var pointList = [];
    var lonlat;
    // copy the geometry out from the nodes
    for (var i=0; i< nodes.length; i++)
    {
            // features are in Map projection, :. reproject
            var newPoint = new OpenLayers.Geometry.Point(nodes[i].x, nodes[i].y); // projected point
            // console.log('in newFeatureAdded - vertex at x:' + nodes[i].x.toString() + " y:" + nodes[i].y.toString());
            var newLatLon = newPoint.transform( map.getProjectionObject(), new OpenLayers.Projection("EPSG:4326"));
            pointList.push(newLatLon);
            // console.log("x: " + newLatLon.x + " y: " + newLatLon.y);
    }

    var geomType = undefined;
    //var pointString = "";
    var pointCoords = [];

    // build the point-list string
    for (j=0; j<pointList.length; j++ ){
            pointCoords.push( pointList[j].y + " " + pointList[j].x );
    }

    // get the type and close the polygon if needed
    switch (geometryClass)
    {
        case "OpenLayers.Geometry.Point":
            geomType = "point";
            break;

        case "OpenLayers.Geometry.LineString":
            geomType = "line";
            break;

        case "OpenLayers.Geometry.Polygon":
            geomType = "polygon";
            // close the polygon by repeating first point.
            pointCoords.push( pointList[0].y + " " + pointList[0].x );
            break;
    }

    currentEntry.geometry.push({ "type": geomType, "data": pointCoords.join(' ') });

    drawPreview(currentEntry);

    //var rawXML = rcEntry.ToXML();

    // deactivate the drawing controls.
    for (key in drawControls) {
        var cont = drawControls[key];

        cont.deactivate();
    }

}; 

function getIcon( entry ) {

	var emSymbol = app_Settings.emSymbols['other'];

	if( entry.icon in app_Settings.emSymbols ) {
		emSymbol = app_Settings.emSymbols[entry.icon];
	}

	var symbolSize = new OpenLayers.Size( emSymbol.size.x, emSymbol.size.y );
	var symbolOffset = new OpenLayers.Pixel( - emSymbol.offset.dx, - emSymbol.offset.dy );

	var icon = new OpenLayers.Icon( emSymbol.url, symbolSize, symbolOffset );

	return icon;
}

function drawPreview(entry){
    // draw icon for road closure
    // Point - draw @ point
    // Line - draw @ mid-point
    // Polygon - draw @ centroid
    //console.log("drawPreview() ");
    // draw colour-coded graphic for LINE or POLYGON
    // Point - do not draw (icon already done)
    if (entry){
    	if (entry.geometry[0]){
    		// Create the Icon - it's needed regardless
			var icon = getIcon( entry );

		    
		    helper = new MASAS.MapHelper();
		    //console.log("in drawPreview - geometry type: " + entry.geometry[0].type);
		    switch (entry.geometry[0].type) 
			{
		        case "point":
		        	
		        	helper.DrawPoint(entry.geometry[0].data, icon);
		        	break;
		                
		        case "line":
		        	
		        	helper.DrawLine(entry.geometry[0].data, entry.colourCode, icon);
		        	break;
		                
		        case "polygon":
		        	
		        	helper.DrawPolygon(entry.geometry[0].data, entry.colourCode, icon);
		        	break;
		        case "circle":
		        	alert ("NOT IMPLEMENTED");
		        	break;
		        case "box":
		        	alert("NOT IMPLEMENTED");
		        	break;
		        	
			}
    	}
    	
    }                  
};

//TODO: create decorator for Marker that uses the .colourCode value 
// (e.g. colour a square or circle in Bottom-right corner of the icon. )


function handleDrawCreated(e){
	//console.log('draw:created fired');
	var type = e.layerType;
	var pointList = [];
	var point = null;
	var radiusM = null;
	var geometryGeoRSS = "";
	var typeGeoRSS = "";
	
	doodleLayer = e.layer;
	
	//console.log('in handleDrawCreated() e.layerType: ' + type);

	if (type === 'marker') {
		//console.log('marker (point) feature');
		typeGeoRSS = "point";
		
		//for (i = 0; i < e.layer.)
		point = doodleLayer.getLatLng();
		
		//console.log('in handleDrawCreated() marker lat: ' + point.lat.toString() + " long: " + point.lng.toString());
		
		//layer.bindPopup('A popup!');
	} else if (type === 'circle') {
		typeGeoRSS = "circle";
		
		point = doodleLayer.getLatLng();
		radiusM = doodleLayer.getRadius(); // in metres
		
		//console.log('in handleDrawCreated() CIRCLE - pt: ' + point.toString() + " radius: " + radiusM.toString() + " m.");
		
	} else if ( (type === 'line') || (type === 'polyline')) {
		typeGeoRSS = "line";
		pointList = doodleLayer.getLatLngs();
		
		
		
		
		//console.log('in handleDrawCreated() LINE ' + " coords:" + pointList.toString());
		
	} else { // polygon or rect (treat as same.)
		typeGeoRSS = "polygon";
		//TODO: Consider RECT->BOX for geometry - treat as polygon for now.
		pointList = doodleLayer.getLatLngs();
		//console.log('in handleDrawCreated() ' + type + " coords:" + pointList.toString());
		
		// need to duplicate last point.
		pointList.splice( (pointList.length), 0, pointList[0] );
		//console.log('in handleDrawCreated() after splice: ' + pointList.toString());
		
		
	}
	
	// handles georss:point and georss:circle
	if (point) {
		geometryGeoRSS = point.lat.toString() + " " + point.lng.toString() ;
	}
	// georss:circle only
	if (radiusM) {
		geometryGeoRSS += " " + radiusM.toString();
	}
	if (pointList.length > 0){
		for (i = 0; i < pointList.length; i++){
		  geometryGeoRSS += pointList[i].lat + " " + pointList[i].lng + " ";
		}
		geometryGeoRSS = geometryGeoRSS.trim();
	}
	
	//console.log('<georss:' + typeGeoRSS + ">" + geometryGeoRSS + '</georss:' + typeGeoRSS + ">" );
	
	newFeatureAdded(typeGeoRSS, geometryGeoRSS);

	//mapDrawnItems.addLayer(doodleLayer);
};


function handleDrawStart(e){
	//console.log('draw:drawstart fired');
	// TODO: remove the doodleLayer doodleLayer
	helper.ClearMap();
	
	
};



