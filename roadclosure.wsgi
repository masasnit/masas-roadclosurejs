import sys, os, bottle

sys.path = ['/var/www/roadclosure/'] + sys.path
os.chdir(os.path.dirname(__file__))

import runwsgi #import the app

application = bottle.default_app()
