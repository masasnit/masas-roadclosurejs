#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:       MASAS Road Closure Tool
Author:     Darrell O'Donnell, P.Eng. (darrell.odonnell@continuumloop.com)
Copyright:  Independent Joint Copyright (c) 2011 MASAS Contributors.
                    Released under the Modified BSD license.  See license.txt for
                    the full text of the license.
Created:    18SEP2013
Updated:    
Description: Provides basic web server using bottle.py for the MASAS Road Closure tools.

"""

import urllib2
from bottle import route, run, get, post, put, static_file, request, response

open511Url = "https://open511.example.org/api"
open511Token = "{TOKEN}"

@route('/')
def serve_homepage():
    return static_file('road-closure-openlayers.html',root='./')

@route('/<filename:path>')
def static(filename):
    return static_file(filename,root='./')


# NOTE: @route wasn't working here - only the GET was making it in. No idea why.
@get('/go')
@post('/go')
@put('/go')
@route('/go', 'PATCH')
def proxyToMASAS():
    print "in proxyToMASAS()"
    url = request.query.get("url")
    
    if (url == None):
        # raise error 
        response.headers["Content-Type"] = "text/plain"
        return "\nIllegal Request, Error: 1"
    if not url.startswith("https://"):
        response.headers["Content-Type"] = "text/plain"
        return "\nIllegal Request, Error: 2"
    print request.method + " " + url
    try:

        if (request.method == "POST"):
            # There is a bug in older python versions where only 200
            # is an acceptable response vs 201
            print "POST"

            original_length = int(request.headers["Content-Length"])
            new_headers = {"Content-Type": request.headers["Content-Type"],
                "User-Agent": "MASAS Road Closure Proxy"}

            if( url.startswith( open511Url ) ):
                new_headers["Authorization"] = "Token " + open511Token

            original_body = request.body.read()
            print original_body
            print "printed original body"
            new_request = urllib2.Request(url, original_body, new_headers)
        elif request.method == "PUT":
            original_length = int(request.headers["Content-Length"])
            new_headers = {"Content-Type": request.headers["Content-Type"],
                "User-Agent": "MASAS Road Closure Proxy"}

            if( url.startswith( open511Url ) ):
                new_headers["Authorization"] = "Token " + open511Token

            original_body = request.body.read()
            print original_body
            new_request = urllib2.Request(url, original_body, new_headers)
            # consider using httplib2 which has builtin PUT method instead
            new_request.get_method = lambda: 'PUT'
        elif request.method == "PATCH":
            original_length = int(request.headers["Content-Length"])
            new_headers = {"Content-Type": request.headers["Content-Type"],
                "User-Agent": "MASAS Road Closure Proxy",
                "X-HTTP-Method-Override": "PATCH" }

            if( url.startswith( open511Url ) ):
                new_headers["Authorization"] = "Token " + open511Token

            original_body = request.body.read()
            print original_body
            new_request = urllib2.Request(url, original_body, new_headers)
            # consider using httplib2 which has builtin PUT method instead
            #new_request.get_method = lambda: 'PATCH'
        else:
            new_headers = {"User-Agent": "MASAS Road Closure Proxy"}
            new_request = urllib2.Request(url, headers=new_headers)
        new_connection = urllib2.urlopen(new_request, timeout=60)
        new_info = new_connection.info()
        response.headers["Content-Type"] = \
            new_info.get("Content-Type", "text/plain")
        response.headers["Location"] = new_info.get("Location", None)
        # 10 MB max page size
        new_data = new_connection.read(10000000)
        new_connection.close()
        return new_data
    except Exception, err:
        response.status = 500
        response.headers["Content-Type"] = "text/plain"
        return "\nProxy Error:\n\n%s" %err
        
    
    #print request.headers
    #TODO: discuss 200/201 python issue raised in the server.py implementations that Jake did.
    
    #print request.method
    
    #return None


run (host='0.0.0.0',port=8888)
