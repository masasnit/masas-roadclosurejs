# TO DO ITEMS:

## High Priority (A)

## Medium Priority (B)

# DONE

# MOVE TO README.md

Road Closure config:

Set Apache to serve the folder and set up the roadclosure.wsgi for the proxy.

```
#!apache directive

<Directory /var/www/roadclosure>
	Order allow,deny
	Allow from all
</Directory>
WSGIScriptAlias /roads /var/www/roadclosure/roadclosure.wsgi
```

The only real variable that is needed is to control the proxy location in ./js/road-closure-app-settings.js to point at the correct endpoint for the proxy.